import { use, useCallback, useMemo, useState, useRef, LC } from '@use-gpu/live';
import { HTML } from '@use-gpu/react';
import React from 'react';

import { NumberInput } from './NumberInput';
import type { Update } from '@use-gpu/state';
import type { RecordState } from './Record';

type RecordControlsProps = {
  recording: boolean,
  frame: number,
  timestamp: number,
  record: () => void,
  stop: () => void,
  options: RecordState,
  onOptionsChange?: (u: Update<RecordState>) => void,
};

const clamp = (x: number, a: number, b: number) => Math.max(a, Math.min(b, x));

export const RecordControls: LC<RecordControlsProps> = (props: RecordControlsProps) => {
  const {
    options,
    recording,
    frame,
    timestamp,
    record,
    stop,
    onOptionsChange,
  } = props;
  
  const {width, height, dpi, from, to, fps} = options;
  const root = document.body;

  return use(HTML, {
    root,
    style: {
      position: 'absolute',
      right: '0px',
      bottom: '0px',
    },
    children: (
      <div style={{
        padding: 10,
        background: '#000000C0',
      }}>
        <Row>
          <Label>Size</Label>
          <NumberInput width={50} value={width} onChange={(width) => onOptionsChange({width})} />
          x
          <NumberInput width={50} value={height} onChange={(height) => onOptionsChange({height})}  />
          @
          <NumberInput width={30} value={dpi} onChange={(dpi) => onOptionsChange({dpi})}  />
          x
        </Row>
        <Row>
          <Label>Time</Label>
          <NumberInput width={50} value={from} onChange={(from) => onOptionsChange({from})} />
          s
          to
          <NumberInput width={50} value={to} onChange={(to) => onOptionsChange({to})}  />
          s @
          <NumberInput width={30} value={fps} onChange={(fps) => onOptionsChange({fps})}  />
          fps
        </Row>
        <Row>
          <button onClick={recording ? stop : record}>{recording ? 'Stop' : 'Record'}</button>
          <div>{recording ? `Frame ${frame + 1} @ ${timestamp.toFixed(2)}` : null}</div>
        </Row>
      </div>
    ),
  });
};

export const Row = (props) => <div style={{display: 'flex', alignItems: 'center', padding: '4px 0px'}}>{props.children}</div>;
export const Label = (props) => <div style={{width: 100}}>{props.children}</div>;
