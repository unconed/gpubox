import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture,
  useDeviceContext, useFontContext, DebugProvider,
  Animate, KeyFrame, Loop,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex,
} from '@use-gpu/layout';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const seq = (n: number, s: number = 0, d: number = 1): number[] => Array.from({ length: n }).map((_, i: number) => s + d * i);

const keyframes = [
  [0, 350],
  [5, 400],
  [10, 350],
] as KeyFrame<any>[];

export const Rounding: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  const [ww] = useResize();

  const glyph = '@';
  const [glyphId] = rustText.findGlyph(0, glyph);
  const glyphMetrics = rustText.measureGlyph(0, glyphId ?? 5, DETAIL * 1.5);

  const {width, height, image} = glyphMetrics;
  const padded = getPaddedSize(width, height, RADIUS);
  const [paddedWidth, paddedHeight] = padded;

  const scale = ww / 430;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = true;
  const contours = false;
  const preprocess = true;
  const postprocess = true;

  const border = scale * 2;
  const padding = scale * 8;
  const fontSize = scale * 15;

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>
      <Loop>

        <LinearRGB>
          <Flat>
            <Pass>
              <UI>
                <Layout>
                  <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'} snap={false}>

                      <Animate keyframes={keyframes} prop="width" render={(width) => (
                        <Block snap={false} width={width * scale} border={[border, border, border, border]} stroke={[1, 1, 1, .2]} padding={[padding * 2, padding]}>
                          <Inline margin={10} align="justify-center" snap={false}>
                            <Text color="#fff" size={fontSize} detail={16}>Five quacking zephyrs jolt my wax bed. Pack my box with five dozen liquor jugs. When zombies arrive, quickly fax judge Pat.</Text>
                          </Inline>
                        </Block>
                      )} />

                  </Flex>
                </Layout>
              </UI>
            </Pass>
          </Flat>
        </LinearRGB>

      </Loop>
    </DebugProvider>
  );
};

