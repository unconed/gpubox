import React, { LC } from '@use-gpu/live';

import {
  LinearRGB, Pass, Flat, RawTexture,
  useDeviceContext, useFontContext, DebugProvider,
  useForceUpdate,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex,
} from '@use-gpu/layout';
import { padRGBA } from '@use-gpu/glyph';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const LINEAR_SAMPLER = {
  minFilter: 'linear',
  magFilter: 'linear',
};

export const EmojiPremultiply2: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  const [, forceUpdate] = useForceUpdate();
  useResize();

  const glyph = '🫕';
  const [fontId] = rustText.resolveFontStack([{ family: 'Noto Emoji' }]);
  const [glyphId, loaded] = rustText.findGlyph(fontId, glyph);
  if (!loaded) {
    rustText.loadMissingGlyph(fontId, glyphId, forceUpdate);
    return null;
  }

  const glyphMetrics = rustText.measureGlyph(fontId, glyphId, DETAIL * 1.5);

  const {width, height, image} = glyphMetrics;

  const padded = getPaddedSize(width, height, RADIUS);
  const [paddedWidth, paddedHeight] = padded;

  const scale = window.innerWidth / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = true;
  const contours = false;
  const preprocess = false;
  const postprocess = true;


  const debugs: Image[] = [];
  const pushDebug = (image: Image) => debugs.push(image);

  const rgbaData = padRGBA(image, width, height, RADIUS).data;

  const rgbaTexture = {
    data: rgbaData,
    format: "rgba8unorm" as GPUTextureFormat,
    colorSpace: 'native',
    size: padded,
  } as DataTexture;

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <Flat>
        <Pass>
          <UI>
            <Layout>
              <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                <RawTexture data={rgbaTexture} sampler={LINEAR_SAMPLER} premultiply render={(texture) =>
                  <Block width={w}>
                    <Block width={w} height={h} fill={[1.0, 1.0, 1.0, 1.0]} image={{
                      texture,
                      fit: 'scale',
                      repeat: 'none',
                    }} />
                  </Block>
                }/>

              </Flex>
            </Layout>
          </UI>
        </Pass>
      </Flat>

    </DebugProvider>
  );
};

