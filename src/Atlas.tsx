import React, { LC, Gather, Yeet } from '@use-gpu/live';

import {
  LinearRGB, Pass, Flat, DebugAtlas,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex, Absolute,
} from '@use-gpu/layout';

import { Pannable } from './Pannable';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

export const Atlas: LC = () => {
  useResize();
  const fontSize = window.innerWidth / 50;
  return (
    <LinearRGB>
      <Pannable
        minZoom={0.125}
        maxZoom={8}
      >
        <Pass>
          <UI>
            <Gather then={
              (layout: any) => [
                <Yeet>{layout}</Yeet>,
                <DebugAtlas size={window.innerWidth / 2} />,
              ]
            }>
              <Layout>
                <Absolute
                  left={10}
                  top={window.innerWidth / 2 + 30}
                  right={10}
                  bottom={10}
                >
                  <Inline align='justify-start'>
                    <Text
                      size={fontSize}
                      detail={64}
                      snap={false}
                      text="A simple and efficient method"
                      weight="bold"
                      color={[0.5, 0.5, 0.5, 1]}
                    />
                    <Text
                      size={fontSize}
                      detail={64}
                      snap={false}
                      family="Lato, Noto Emoji"
                      text={" is ⭐️✨✌️🌿🌳🐲🐬🍅🍲🫕🏀🏈🏴‍☠️👨‍🚀❤️‍🔥 presented which allows improved rendering of glyphs composed of curved and linear elements. A sub-pixel distance field is generated from a glyph image, and then stored into a channel of an RGBA texture.\n\nIn the simplest case, this texture can then be rendered simply by using the alpha-testing and alpha-thresholding feature of modern GPUs, without a custom shader. This allows the technique to be used on even the lowest-end 3D graphics hardware."}
                      color={[0.5, 0.5, 0.5, 1]}
                    />
                  </Inline>
                </Absolute>
              </Layout>
            </Gather>
          </UI>
        </Pass>
      </Pannable>
    </LinearRGB>
  );
};

