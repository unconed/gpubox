import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture, Scissor,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  Cartesian, Axis, Grid, Sampled, Line, Point,
} from '@use-gpu/plot';

import { Label } from './Label';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const DATA = [0, 0, 1, 4, 9, 4, 4, 4, 1, 1, 4, 9, 4, 9, 9];

const sampleParabola = (x: number, i: number) => {
  const y = (i - x) * (i - x) + DATA[i];
  return y;
};

const sampleParabolas = (x: number) => {
  let y = Infinity;
  for (let i = 0, n = DATA.length; i < n; ++i) {
    const d = sampleParabola(x, i);
    if (Number.isNaN(d)) continue;
    y = Math.min(y, d);
  }
  return y;
};

export const Parabola1D: LC = () => {
  useResize();

  const w = window.innerWidth;
  const h = window.innerHeight;

  return (
    <LinearRGB>
      <Flat x={w/2} y={h/2}>
        <Pass>
          <Cartesian
            range={[[0, 14], [0, 10]]}
            scale={[w / 2 - 20, -h / 2 + 20]}
          >
            <Axis axis="x" />
            <Axis axis="y" />
            <Grid axes="xy" color='#444' zBias={-1} />

            <Scissor range={[[0, 14.001], [0, 10]]}>
              <Sampled
                axis="xy"
                size={[300, DATA.length]}
                format="vec2<f32>"
                index
                expr={(emit, x, y, i, j) => {
                  emit(x, sampleParabola(x, j))
                }}
              >
                <Line
                  width={2}
                  color="#888"
                />
              </Sampled>

              <Sampled
                axis="x"
                size={[300]}
                format="vec2<f32>"
                index
                expr={(emit, x) => {
                  emit(x, sampleParabolas(x))
                }}
              >
                <Line
                  width={4}
                  color="#6af"
                  zBias={1}
                />
              </Sampled>

              <Sampled
                axis="x"
                size={[14]}
                format="vec2<f32>"
                index
                expr={(emit, x, i) => {
                  emit(i, sampleParabola(i, i))
                }}
              >
                <Point
                  size={8}
                  color="#888"
                  zBias={0}
                />
              </Sampled>

              <Sampled
                axis="x"
                size={[14]}
                format="vec2<f32>"
                index
                expr={(emit, x, i) => {
                  emit(i, sampleParabolas(i))
                }}
              >
                <Point
                  size={8}
                  color="#fff"
                  zBias={4}
                />
              </Sampled>

            </Scissor>
          </Cartesian>
        </Pass>
      </Flat>
    </LinearRGB>
  );
};

