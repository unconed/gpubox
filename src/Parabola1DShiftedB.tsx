import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture, Scissor,
  useDeviceContext, useFontContext, DebugProvider,
  Animate, Loop, Keyframe,
} from '@use-gpu/workbench';
import {
  Cartesian, Axis, Grid, Sampled, Line, Point,
} from '@use-gpu/plot';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const DATA  = [100, 100, 100, 100, 0, 0, 0, 0, 0, 100, 100, 100, 100, 100]
let SHIFT = DATA.map(_ => 0);

const keyframes = [
  [0, -.5],
  [2, .5],
  [4, -.5],
];

const sampleParabola = (x: number, i: number) => {
  const ii = i + SHIFT[i];
  const y = (ii - x) * (ii - x) + DATA[i];
  return y;
};

const sampleParabolas = (x: number) => {
  let y = Infinity;
  for (let i = 0, n = DATA.length; i < n; ++i) {
    const d = sampleParabola(x, i);
    if (Number.isNaN(d)) continue;
    y = Math.min(y, d);
  }
  return y;
};

export const Parabola1DShiftedB: LC = () => {
  const [w, h] = useResize();

  return (
    <Loop>
      <Animate keyframes={keyframes} prop={"shift"} render={(shift) => {
        SHIFT = DATA.map((_, i) => {
          return i === 4 || i === 8 ? shift : 0;
        });
        return (
          <LinearRGB>
            <Flat x={w/2} y={h/2}>
              <Pass>
                <Cartesian
                  range={[[0, 14], [0, 10]]}
                  scale={[w / 2 - 10, -h / 2 + 10]}
                >
                  <Axis axis="x" />
                  <Axis axis="y" />
                  <Grid axes="xy" color='#444' zBias={-1} />

                  <Scissor range={[[0, 14], [0, 10]]}>
                    <Sampled
                      axis="xy"
                      size={[300, DATA.length]}
                      format="vec2<f32>"
                      index
                      expr={(emit, x, y, i, j) => {
                        emit(x, sampleParabola(x, j))
                      }}
                    >
                      <Line
                        width={2}
                        color="#888"
                      />
                    </Sampled>

                    <Sampled
                      axis="x"
                      size={[300]}
                      format="vec2<f32>"
                      index
                      expr={(emit, x) => {
                        emit(x, sampleParabolas(x))
                      }}
                    >
                      <Line
                        width={4}
                        color="#6af"
                        zBias={1}
                      />
                    </Sampled>

                    <Sampled
                      axis="x"
                      size={[14]}
                      format="vec2<f32>"
                      index
                      expr={(emit, x, i) => {
                        emit(i + SHIFT[i], sampleParabola(i + SHIFT[i], i))
                      }}
                    >
                      <Point
                        size={8}
                        color="#888"
                        zBias={1}
                      />
                    </Sampled>

                    <Sampled
                      axis="x"
                      size={[14]}
                      format="vec2<f32>"
                      index
                      expr={(emit, x, i) => {
                        emit(i, sampleParabolas(i))
                      }}
                    >
                      <Point
                        size={8}
                        color={[1, 1, 1, 1]}
                        zBias={4}
                      />
                    </Sampled>

                    <Sampled
                      axis="x"
                      size={[14]}
                      sparse
                      format="vec2<f32>"
                      index
                      expr={(emit, x, i) => {
                        if (i === 0) emit(-100, -100);
                        else if (
                          (i === 4 && SHIFT[i] < 0) ||
                          (i === 8 && SHIFT[i] > 0)
                        ) {
                          emit(i, sampleParabolas(i))
                        }
                      }}
                    >
                      <Point
                        size={20}
                        stroke={2}
                        shape="circleOutlined"
                        color={[1, 0, 0, 1]}
                        zBias={5}
                      />
                    </Sampled>
                  </Scissor>
                </Cartesian>
              </Pass>
            </Flat>
          </LinearRGB>
        )
      }} />
    </Loop>
  );
};

