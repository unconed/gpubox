import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture,
  useDeviceContext, useFontContext, useBoundSource, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex, Absolute, Transform,
} from '@use-gpu/layout';

import { Label } from './Label';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const CLIP = {
  name: 'clip',
  format: 'vec4<f32>',
  args: ['u32'],
};

export const GlyphEDTContoursT: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  useResize();

  const glyph = 'T';
  const [glyphId] = rustText.findGlyph(0, glyph);
  const glyphMetrics = rustText.measureGlyph(0, glyphId ?? 5, DETAIL * 1.5);

  const {width, height, image} = glyphMetrics;
  const padded = getPaddedSize(width, height, RADIUS);
  const [paddedWidth, paddedHeight] = padded;

  const scale = window.innerWidth / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = false;
  const contours = true;
  const preprocess = false;
  const postprocess = false;

  const sdfData = glyphToSDF(image, width, height, RADIUS, RADIUS, undefined, subpixel, true, preprocess, postprocess).data;

  const sdfTexture = {
    data: sdfData,
    format: "rgba8unorm" as GPUTextureFormat,
    colorSpace: 'srgb',
    size: padded,
  } as DataTexture;

  const fontSize = DETAIL * scale * 1.5;

  const rectLeft = [0, 0, w/2, h];
  const rectRight = [w/2, 0, w, h];
  const clipLeft = useBoundSource(CLIP, rectLeft);
  const clipRight = useBoundSource(CLIP, rectRight);

  return (<>
    <LinearRGB>

        <Flat x={-w/4} y={-h/2} zoom={2}>
          <Pass>
            <DebugProvider debug={{sdf2d: {subpixel: false, contours, preprocess, postprocess}}}>
              <UI>
                <Layout>
                  <Transform clip={clipLeft}>
                    <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                      <Inline align={"center"}>
                        <Text color="#fff" size={fontSize} detail={64} lineHeight={fontSize * 0.3}>{glyph}</Text>
                      </Inline>

                    </Flex>
                  </Transform>
                </Layout>
              </UI>
            </DebugProvider>
            <DebugProvider debug={{sdf2d: {subpixel: true, contours, preprocess, postprocess}}}>
              <UI>
                <Layout>
                  <Transform clip={clipRight}>
                    <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                      <Inline align={"center"}>
                        <Text color="#fff" size={fontSize} detail={64} lineHeight={fontSize * 0.3}>{glyph}</Text>
                      </Inline>

                    </Flex>
                  </Transform>
                </Layout>
              </UI>
            </DebugProvider>
            <UI>
              <Layout>
                <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>
                  <Block width={2} height={"2000"} fill={[1, 1, 1, .65]} />
                </Flex>
              </Layout>
            </UI>
          </Pass>
        </Flat>

        <Flat>
          <Pass overlay>
            <UI>
              <Layout>
                <Absolute left={0} bottom={0}>
                  <Block padding={[10, 5, 10, 5]} fill={[0, 0, 0, .95]}>
                    <Label size={20} weight={'bold'}>TinySDF</Label>
                  </Block>
                </Absolute>
                <Absolute right={0} bottom={0}>
                  <Block padding={[10, 5, 10, 5]} fill={[0, 0, 0, .95]}>
                    <Label size={20} weight={'bold'}>Use.GPU</Label>
                  </Block>
                </Absolute>
              </Layout>
            </UI>
          </Pass>
        </Flat>
    </LinearRGB>
  </>);
};

