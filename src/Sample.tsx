import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex,
} from '@use-gpu/layout';

import { SIZE, DETAIL, RADIUS } from './constants';

export const Sample: LC = () => {

  const subpixel = true;
  const contours = false;
  const preprocess = true;
  const postprocess = true;

  const scale = window.innerWidth / 30;
  const fontSize = scale * 1.5;

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <LinearRGB>
        <Flat>
          <Pass>
            <UI>
              <Layout>
                <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                  <Inline margin={10} align="center">
                    <Text color="#fff" size={fontSize} family="Lato, Noto Emoji">{"Hello World   ⭐️✨✌️🌿🌳🐲🐬🍅🍲🫕\n"}</Text>
                  </Inline>

                </Flex>
              </Layout>
            </UI>
          </Pass>
        </Flat>
      </LinearRGB>

    </DebugProvider>
  );
};

