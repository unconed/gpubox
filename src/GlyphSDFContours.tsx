import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex, Absolute,
} from '@use-gpu/layout';

import { Pannable } from './Pannable';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

export const GlyphSDFContours: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  useResize();

  const glyph = '@';
  const [glyphId] = rustText.findGlyph(0, glyph);
  const glyphMetrics = rustText.measureGlyph(0, glyphId ?? 5, DETAIL * 1.5);

  const {width, height, image} = glyphMetrics;
  const padded = getPaddedSize(width, height, RADIUS);
  const [paddedWidth, paddedHeight] = padded;

  const scale = window.innerWidth / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = true;
  const contours = true;
  const preprocess = true;
  const postprocess = true;

  const fontSize = DETAIL * scale * 1.5;

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <LinearRGB>
        <Pannable>
          <Pass>
            <UI>
              <Layout>
                <Absolute top={"-40%"}>
                  <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                    <Inline align={"center"}>
                      <Text color="#fff" size={fontSize} detail={64} lineHeight={0}>{glyph}</Text>
                    </Inline>

                  </Flex>
                </Absolute>
              </Layout>
            </UI>
          </Pass>
        </Pannable>
      </LinearRGB>
    </DebugProvider>
  );
};

