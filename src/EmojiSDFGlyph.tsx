import React, { LC } from '@use-gpu/live';

import {
  LinearRGB, Pass, Flat, RawTexture,
  useDeviceContext, useFontContext, DebugProvider,
  useForceUpdate,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex,
} from '@use-gpu/layout';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

export const EmojiSDFGlyph: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  const [, forceUpdate] = useForceUpdate();
  useResize();

  const glyph = '🫕';
  const [fontId] = rustText.resolveFontStack([{ family: 'Noto Emoji' }]);
  const [glyphId, loaded] = rustText.findGlyph(fontId, glyph);
  if (!loaded) {
    rustText.loadMissingGlyph(fontId, glyphId, forceUpdate);
    return null;
  }

  const glyphMetrics = rustText.measureGlyph(fontId, glyphId, DETAIL * 1.5);

  const {width, height, image} = glyphMetrics;

  const padded = getPaddedSize(width, height, RADIUS);
  const [paddedWidth, paddedHeight] = padded;

  const scale = window.innerWidth / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = true;
  const contours = false;
  const preprocess = false;
  const postprocess = true;

  const fontSize = DETAIL * scale * 1.25;

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <LinearRGB>
        <Flat>
          <Pass>
            <UI>
              <Layout>
                <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                  <Inline align={"center"}>
                    <Text family="Noto Emoji" color="#fff" size={fontSize} detail={64} lineHeight={fontSize * 0.7}>{glyph}</Text>
                  </Inline>

                </Flex>
              </Layout>
            </UI>
          </Pass>
        </Flat>
      </LinearRGB>

    </DebugProvider>
  );
};

