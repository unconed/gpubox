import React, { LC, hot, into, useFiber, useOne, useResource } from '@use-gpu/live';

import { Router, Routes } from '@use-gpu/workbench';
import { AutoCanvas, WebGPU } from '@use-gpu/webgpu';
import { FontLoader } from '@use-gpu/workbench';
//import { UseInspect } from '@use-gpu/inspect';
//import { inspectGPU } from '@use-gpu/inspect-gpu';
import mapValues from 'lodash/mapValues';

import '@use-gpu/inspect/theme.css';
import NOTO_SEQUENCES from './noto-emoji.json';

import { Atlas } from './Atlas';
import { GlyphSDF } from './GlyphSDF';
import { GlyphSDFContours } from './GlyphSDFContours';
import { GlyphContrast } from './GlyphContrast';
import { GlyphContrastShift } from './GlyphContrastShift';
import { GlyphEDTSDF } from './GlyphEDTSDF';
import { GlyphEDTContours } from './GlyphEDTContours';
import { GlyphEDTContoursCompare } from './GlyphEDTContoursCompare';
import { GlyphEDTContoursT } from './GlyphEDTContoursT';
import { GlyphEDTFieldX } from './GlyphEDTFieldX';
import { GlyphEDTFieldXY } from './GlyphEDTFieldXY';
import { EmojiRGBA } from './EmojiRGBA';
import { EmojiPremultiply1 } from './EmojiPremultiply1';
import { EmojiPremultiply2 } from './EmojiPremultiply2';
import { EmojiSDF } from './EmojiSDF';
import { EmojiSDFA } from './EmojiSDFA';
import { EmojiSDFContours } from './EmojiSDFContours';
import { EmojiSDFGlyph } from './EmojiSDFGlyph';
import { ESDTOffsets } from './ESDTOffsets';
import { ESDTOffsetsWedge } from './ESDTOffsetsWedge';
import { ESDTOffsetsRelax } from './ESDTOffsetsRelax';
import { ESDTBorder } from './ESDTBorder';
import { ESDTFieldX } from './ESDTFieldX';
import { ESDTFieldXY } from './ESDTFieldXY';
import { ESDTFieldXYRelax } from './ESDTFieldXYRelax';
import { ESDTFieldXYRelaxCompare } from './ESDTFieldXYRelaxCompare';
import { Parabola1D } from './Parabola1D';
import { Parabola1DFlat } from './Parabola1DFlat';
import { Parabola1DXY } from './Parabola1DXY';
import { Parabola1DShifted } from './Parabola1DShifted';
import { Parabola1DShiftedA } from './Parabola1DShiftedA';
import { Parabola1DShiftedB } from './Parabola1DShiftedB';
import { PixelsRow } from './PixelsRow';
import { PixelsRowGrey } from './PixelsRowGrey';
import { Rounding } from './Rounding';
import { Sample } from './Sample';
import { Scales } from './Scales';
import { VoronoiSlope } from './VoronoiSlope';
import { VoronoiSlopeGrey } from './VoronoiSlopeGrey';
import { VoronoiSlopeAA } from './VoronoiSlopeAA';
import { VoronoiSlope3D } from './VoronoiSlope3D';
import { VoronoiSlope3DB } from './VoronoiSlope3DB';
import { VoronoiDiagonal } from './VoronoiDiagonal';
import { VoronoiCommute1 } from './VoronoiCommute1';
import { VoronoiCommute2 } from './VoronoiCommute2';

import { FALLBACK_MESSAGE, Video, Image } from './Fallback';

export const base = location.host === 'localhost:4000' || location.host === 'acko.net' ? "/files/gpubox/" : "/";

//const inspect = location.hostname === 'localhost';
const getNotoEmojiURL = (name: string) => `${base}fonts/emoji/emoji_u${name}.png`;

const ROUTES = {
  '/atlas': {element: <Atlas />, fallback: <Image id="atlas" />},
  '/glyph/sdf': {element: <GlyphSDF />},
  '/glyph/sdf-contours': {element: <GlyphSDFContours />, fallback: <Image id="esdt-contours" retina />},
  '/glyph/contrast': {element: <GlyphContrast />, fallback: <Video id="sdf-contrast" />},
  '/glyph/contrast-shift': {element: <GlyphContrastShift />, fallback: <Video id="sdf-contrast-shift" />},
  '/glyph/edt-sdf': {element: <GlyphEDTSDF />},
  '/glyph/edt-contours': {element: <GlyphEDTContours />, fallback: <Image id="glyph-sdf-edt-contours" retina />},
  '/glyph/edt-contours-compare': {element: <GlyphEDTContoursCompare />},
  '/glyph/edt-contours-t': {element: <GlyphEDTContoursT />},
  '/glyph/edt-x': {element: <GlyphEDTFieldX />},
  '/glyph/edt-xy': {element: <GlyphEDTFieldXY />},
  '/emoji/rgba': {element: <EmojiRGBA />},
  '/emoji/premultiply1': {element: <EmojiPremultiply1 />},
  '/emoji/premultiply2': {element: <EmojiPremultiply2 />},
  '/emoji/sdf': {element: <EmojiSDF />},
  '/emoji/sdfa': {element: <EmojiSDFA />},
  '/emoji/sdf-contours': {element: <EmojiSDFContours />, fallback: <Image id="emoji-sdf-contours" retina />},
  '/emoji/sdf-glyph': {element: <EmojiSDFGlyph />},
  '/esdt/offsets': {element: <ESDTOffsets />, fallback: <Image id="esdt-offsets" />},
  '/esdt/offsets-wedge': {element: <ESDTOffsetsWedge />, fallback: <Image id="esdt-offsets-wedge" />},
  '/esdt/offsets-relax': {element: <ESDTOffsetsRelax />, fallback: <Image id="esdt-offsets-relax" />},
  '/esdt/x': {element: <ESDTFieldX />, fallback: <Image id="esdt-x" />},
  '/esdt/xy': {element: <ESDTFieldXY />, fallback: <Image id="esdt-xy" />},
  '/esdt/xy-relax': {element: <ESDTFieldXYRelax />, fallback: <Image id="esdt-xy-relax" />},
  '/esdt/xy-relax-compare': {element: <ESDTFieldXYRelaxCompare />, fallback: <Image id="esdt-xy-diagonal" />},
  '/esdt/border': {element: <ESDTBorder />, fallback: <Image id="esdt-border" />},
  '/parabola/1d': {element: <Parabola1D />},
  '/parabola/1d-flat': {element: <Parabola1DFlat />},
  '/parabola/1d-xy': {element: <Parabola1DXY />, fallback: <Image id="parabola-1d-xy" />},
  '/parabola/1d-shifted': {element: <Parabola1DShifted />, fallback: <Video id="parabola-1d-shifted" />},
  '/parabola/1d-shifted-a': {element: <Parabola1DShiftedA />, fallback: <Video id="parabola-1d-shifted-a" />},
  '/parabola/1d-shifted-b': {element: <Parabola1DShiftedB />, fallback: <Video id="parabola-1d-shifted-b" />},
  '/pixels/row': {element: <PixelsRow />},
  '/pixels/row-grey': {element: <PixelsRowGrey />},
  '/rounding': {element: <Rounding />, fallback: <Video id="rounding" retina />},
  '/sample': {element: <Sample />},
  '/scales': {element: <Scales />},
  '/voronoi/slope': {element: <VoronoiSlope />},
  '/voronoi/slope-grey': {element: <VoronoiSlopeGrey />},
  '/voronoi/slope-aa': {element: <VoronoiSlopeAA />},
  '/voronoi/slope-3d': {element: <VoronoiSlope3D />, fallback: <Image id="voronoi-slope-3d" type="jpg" />},
  '/voronoi/slope-3d-b': {element: <VoronoiSlope3DB />, fallback: <Image id="voronoi-slope-3d-b" type="jpg" />},
  '/voronoi/diagonal': {element: <VoronoiDiagonal />},
  '/voronoi/commute-1': {element: <VoronoiCommute1 />},
  '/voronoi/commute-2': {element: <VoronoiCommute2 />},
};

const FALLBACK_ROUTES = mapValues(ROUTES, (r) => ({
  ...r,
  element: r.fallback,
}));

export const App: LC = hot(() => {
  const fiber = useFiber();

  const root = document.querySelector('#gpu-box')!;
  const inner = document.querySelector('#gpu-box .canvas')!;

  const fonts = useOne(() => [
    {
      family: 'Lato',
      weight: 400,
      style: 'normal',
      src: base + 'fonts/Lato-Regular.ttf',
    },
    {
      family: 'Lato',
      weight: 500,
      style: 'normal',
      src: base + 'fonts/Lato-Bold.ttf',
    },
    {
      family: 'Noto Emoji',
      weight: 400,
      style: 'normal',
      lazy: {
        sequences: NOTO_SEQUENCES,
        fetch: (index: number) => {
          // name = "XXXX_XXXX_XXXX" where X = codepoint in hex
          const seq = NOTO_SEQUENCES[index];
          const codepoints = [...seq].map(s => s.codePointAt(0)!);
          const name = codepoints.map(i => i.toString(16)).join('_');
          return getNotoEmojiURL(name);
        },
      },
    }
  ]);
  
  const view = (
    <Router base={base} hash>
      <WebGPU
        fallback={(error: Error) =>
            <Routes routes={FALLBACK_ROUTES} />
        }
      >
        <AutoCanvas
          iframe
          selector={'#gpu-box .canvas'}
          samples={4}
        >
          <FontLoader fonts={fonts}>
            <Routes routes={ROUTES} />
          </FontLoader>
        </AutoCanvas>
      </WebGPU>
    </Router>
  );

  useResource((dispose) => {
    const canvas = document.querySelector('canvas');
    const f = (e) => {
      e.stopPropagation();
    };
    document.body.addEventListener('wheel', f, {capture: true});
    dispose(() => document.body.removeEventListener('wheel', f, {capture: true}));
  })

  /*
  if (inspect) {
    return (
      <UseInspect
        fiber={fiber}
        container={root}
        extensions={[inspectGPU]}
        appearance={{toolbar: false}}
      >
        {view}
      </UseInspect>
    );
  }
  */

  return view;
}, module);

App.displayName = 'App';
