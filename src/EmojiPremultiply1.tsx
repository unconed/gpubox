import React, { LC } from '@use-gpu/live';

import {
  Pass, Flat, RawTexture, TextureShader,
  useDeviceContext, useFontContext, DebugProvider,
  useForceUpdate,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex,
} from '@use-gpu/layout';
import { padRGBA } from '@use-gpu/glyph';
import { wgsl } from '@use-gpu/shader/wgsl';

import { Label } from './Label';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const LINEAR_SAMPLER = {
  minFilter: 'linear',
  magFilter: 'linear',
};

const postmultiply = wgsl`
@link fn getTexture(uv: vec2<f32>) -> vec4<f32>;

fn main(uv: vec2<f32>) -> vec4<f32> {
  let c = getTexture(uv);
  return vec4<f32>(c.xyz * c.a, c.a);
}
`;

export const EmojiPremultiply1: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  const [, forceUpdate] = useForceUpdate();
  useResize();

  const glyph = '🫕';
  const [fontId] = rustText.resolveFontStack([{ family: 'Noto Emoji' }]);
  const [glyphId, loaded] = rustText.findGlyph(fontId, glyph);
  if (!loaded) {
    rustText.loadMissingGlyph(fontId, glyphId, forceUpdate);
    return null;
  }

  const glyphMetrics = rustText.measureGlyph(fontId, glyphId, DETAIL * 1.5);

  const {width, height, image} = glyphMetrics;

  const padded = getPaddedSize(width, height, RADIUS);
  const [paddedWidth, paddedHeight] = padded;

  const scale = window.innerWidth / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = true;
  const contours = false;
  const preprocess = false;
  const postprocess = true;


  const debugs: Image[] = [];
  const pushDebug = (image: Image) => debugs.push(image);

  const rgbaData = padRGBA(image, width, height, RADIUS).data;

  for (let y = 0, i = 0; y < paddedHeight; ++y) {
    for (let x = 0; x < paddedWidth; ++x, i += 4) {
      const a = rgbaData[i + 3] / 255;
      if (a === 0) {
        rgbaData[i    ] = 0;
        rgbaData[i + 1] = 0;
        rgbaData[i + 2] = 0;
      }
    }
  }

  const rgbaTexture = {
    data: rgbaData,
    format: "rgba8unorm" as GPUTextureFormat,
    colorSpace: 'srgb',
    size: padded,
  } as DataTexture;

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <Flat>
        <Pass>
          <UI>
            <Layout>
              <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                <RawTexture data={rgbaTexture} sampler={LINEAR_SAMPLER} render={(texture) =>
                  <TextureShader texture={texture} shader={postmultiply} render={(texture) =>
                    <Block width={w}>
                      <Block width={w} height={h} fill={[1.0, 1.0, 1.0, 1.0]} image={{
                        texture,
                        fit: 'scale',
                        repeat: 'none',
                      }} />
                    </Block>
                  } />
                }/>

              </Flex>
            </Layout>
          </UI>
        </Pass>
      </Flat>

    </DebugProvider>
  );
};

