import React, { useOne, useState } from '@use-gpu/live';
import { PanControls, Flat, Cursor } from '@use-gpu/workbench';
import { Zoom } from './Zoom';

type PannableProps = {
  focus?: number,

  minZoom?: number,
  maxZoom?: number,

  initialX?: number,
  initialY?: number,
  initialZoom?: number,
};

export const Pannable = (props: PannableProps) => {
  const {
    focus = 1,
    minZoom = 0.5,
    maxZoom = 5,
    initialX = 0,
    initialY = 0,
    initialZoom = 1,
  } = props;

  const [zoom, setZoom] = useState(initialZoom);
  useOne(() => setZoom(initialZoom), initialZoom);

  return (
    <>
      <PanControls
        active={true}
        minZoom={minZoom}
        maxZoom={maxZoom}
        x={initialX}
        y={initialY}
        zoom={zoom}
        render={(x, y, zoom) =>
          <Flat x={x} y={y} zoom={zoom} focus={focus}>
            <Cursor cursor="move" />
            {props.children}
          </Flat>
        }
      />
      <Zoom
        min={minZoom}
        max={maxZoom}
        value={zoom}
        onChange={setZoom}
      />
    </>
  );
};