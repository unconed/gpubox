import React, { LC } from '@use-gpu/live';

import { glyphToRGBA, glyphToSDF, makeSDFStage, paintSubpixelOffsets } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture, PanControls,
  CompositeData, LineLayer, LineSegments,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex, Embed,
} from '@use-gpu/layout';
import {
  Sampled, Grid, Axis, Cartesian, Embedded, Point, Arrow, Line, Scale, Tick,
} from '@use-gpu/plot';

import { Pannable } from './Pannable';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const ZBIAS_GRID = 3;
const ZBIAS_DATA = 4;

export const ESDTFieldX: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  useResize();

  const glyph = '@';
  const [glyphId] = rustText.findGlyph(0, glyph);
  const glyphMetrics = rustText.measureGlyph(0, glyphId ?? 5, DETAIL * 1.5);

  const debugs: Image[] = [];
  const pushDebug = (image: Image) => debugs.push(image);

  const {width, height, image} = glyphMetrics;
  const radius = RADIUS;
  const padded = getPaddedSize(width, height, radius);
  const [paddedWidth, paddedHeight] = padded;

  const scale = window.innerWidth / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = true;
  const contours = false;
  const preprocess = false;
  const postprocess = true;

  const rgbaData = glyphToRGBA(image, width, height, radius).data;

  glyphToSDF(image, width, height, radius, radius, undefined, subpixel, true, preprocess, postprocess, pushDebug).data;

  const rgbaTexture = {
    data: rgbaData,
    format: "rgba8unorm" as GPUTextureFormat,
    colorSpace: 'srgb',
    size: padded,
  } as DataTexture;

  const s = Math.max(paddedWidth, paddedHeight);
  const sdf1 = makeSDFStage(s);
  const sdf2 = makeSDFStage(s);
  paintSubpixelOffsets(sdf1, image, width, height, radius, preprocess, true);
  paintSubpixelOffsets(sdf2, image, width, height, radius, preprocess, false);

  const {xo, yo, xi, yi} = sdf1;
  const {xo: xo2, yo: yo2, xi: xi2, yi: yi2} = sdf2;

  const arrowEmitter = ({xs, ys, width, height}: DebugImage) =>
    (emit: Emit, x: number, y: number, i: number, j: number) => {
      const index = i + j * paddedWidth;
      const dx = xs[index];
      const dy = ys[index];

      if (dx || dy) {
        emit(x, y, 0, 1);
        emit(x + dx, y + dy, 0, 1);
      }
    };

    const debugFrame = (image: Image) => (
      image ? <TextureFrame texture={{
        data: image.data,
        format: "rgba8unorm",
        colorSpace: 'srgb',
        size: [image.width, image.height],
      }}>

        { image.xi && image.yi ? <Sampled
          axes='xy'
          format='vec4<f32>'
          size={padded}
          items={2}
          sparse
          centered
          index
          expr={arrowEmitter({
            xs: image.xi,
            ys: image.yi,
            width: image.width,
            height: image.height,
          })}
        >
          <Arrow width={2} color={0xffffff} depth={0.01} detail={4} zBias={ZBIAS_DATA} />
        </Sampled> : null}

        { image.xo && image.yo ? <Sampled
          axes='xy'
          format='vec4<f32>'
          size={padded}
          items={2}
          sparse
          centered
          index
          expr={arrowEmitter({
            xs: image.xo,
            ys: image.yo,
            width: image.width,
            height: image.height,
          })}
        >
          <Arrow width={2} color={0xffffff} depth={0.01} detail={4} zBias={ZBIAS_DATA} />
        </Sampled> : null}
      </TextureFrame> : null
    );

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <LinearRGB>
        <Pannable
          initialX={-24}
          initialY={27.00}
          initialZoom={scale * 5.5}
          minZoom={scale}
          maxZoom={scale * 32}
          focus={1/3}
        >
          <Pass>
            <UI>
              <Layout>
                <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                  {debugFrame(debugs[1])}

                </Flex>
              </Layout>
            </UI>
          </Pass>
        </Pannable>
      </LinearRGB>

    </DebugProvider>
  );
};

type TextureFrameProps = {
  texture: any,
  margin?: number,
}

const TextureFrame: LC<TextureFrameProps> = (props: PropsWithChildren<TextureFrameProps>) => {
  const {margin, texture, children} = props;
  const {size: [width, height]} = texture;

  return (
    <RawTexture data={texture} render={(texture) =>
      <Block margin={margin} width={width} height={height} fill={[0.0, 0.0, 0.0, 1.0]} image={{
        texture,
        fit: 'contain',
        repeat: 'none',
      }}>
        <Embed width="100%" height="100%">
          <Embedded>
            {children}
          </Embedded>
        </Embed>
      </Block>
    }/>
  );
}

