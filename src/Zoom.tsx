import { use, useCallback, useMemo, useState, useRef, LC } from '@use-gpu/live';
import { HTML } from '@use-gpu/react';
import React from 'react';

type ZoomProps = {
  size: number,
  color: string,

  min: number,
  max: number,
  value: number,
  onChange?: (z: number) => void,
};

const clamp = (x: number, a: number, b: number) => Math.max(a, Math.min(b, x));

export const Zoom: LC<ZoomProps> = (props: ZoomProps) => {
  const {
    size = 20,
    color = '#fff',

    min = 1,
    max = 2,
    value = 1.5,
    onChange,
  } = props;

  const minLog = Math.log(min);
  const maxLog = Math.log(max);
  const valueLog = Math.log(value);

  const root = document.body;

  const circle = 0.7 * size;
  const stem = 0.3 * size;

  const trackSize = 100;
  const thumbSize = 20;

  const level = clamp((valueLog - minLog) / (maxLog - minLog), 0, 1);
  const top = (trackSize - thumbSize) * (1 - level);

  const [dragging, setDragging] = useState(false);

  const anchorRef = useRef([0, 0]);
  const draggingRef = useRef(dragging);
  const valueRef = useRef(valueLog);
  draggingRef.current = dragging;
  valueRef.current = valueLog;

  const handlePointerDown = useCallback((e: PointerEvent) => {
    (e.target as any).setPointerCapture(e.pointerId);
    setDragging(true);
    anchorRef.current = [e.clientY, valueRef.current];
    e.preventDefault();
  }, []);

  const handlePointerMove = useCallback((e: PointerEvent) => {
    const {current: dragging} = draggingRef;
    if (!dragging) return;

    const {current: [anchor, value]} = anchorRef;
    (e.target as any).setPointerCapture(e.pointerId);

    const dy = (e.clientY - anchor);
    const dl = dy / (trackSize - thumbSize) * (maxLog - minLog);
    onChange?.(Math.exp(clamp(-dl + value, minLog, maxLog)));
    e.preventDefault();
  }, []);

  const handlePointerUp = useCallback((e: PointerEvent) => {
    setDragging(false);
    e.preventDefault();
  }, []);

  return useMemo(() => (
    use(HTML, {
      root,
      style: {
        position: 'absolute',
        right: '5px',
        bottom: '5px',
      },
      children: (
        <div style={{
          padding: 10,
          background: '#000000C0',
          touchAction: 'none',
        }}>
          <div style={{position: 'relative', width: thumbSize, margin: '0 auto'}}>
            <div
              style={{
                margin: '0 auto',
                width: 6,
                borderRadius: 3,
                height: trackSize,
                background: '#ffffff70',
              }}
            />
            <div
              onPointerDown={handlePointerDown}
              onPointerMove={handlePointerMove}
              onPointerUp={handlePointerUp}
              style={{
                position: 'absolute',
                top,
                width: thumbSize,
                height: thumbSize,
                borderRadius: '100%',
                background: !dragging ? '#e0e0e0' : '#ffffff',
              }}
            />
          </div>
          <div style={{
            marginLeft: 2,
            marginTop: 10,
            position: 'relative',
            width: size,
            height: size,
            borderRadius: 4,
          }}>
            <div style={{
              position: 'absolute',
              left: 0,
              width: circle,
              height: circle,
              border: `2px solid ${color}`,
              borderRadius: '100%',
            }} />
            <div style={{
              position: 'absolute',
              left: 0,
              width: stem * 1.414,
              height: '2px',
              background: color,
              transform: `rotate(45deg) translate(${circle * 1.2}px, 0)`,
              transformOrigin: '0px 0px',
            }} />
          </div>
        </div>
      ),
    })
  ), [size, color, dragging, top, trackSize, thumbSize]);
};