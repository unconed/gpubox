export const isMobile = () => window.innerWidth < 768;
export const isNarrow = () => window.innerWidth < 500;

export const getPaddedSize = (width: number, height: number, radius: number) => {
  const paddedWidth = width + radius * 2;
  const paddedHeight = height + radius * 2;
  return [paddedWidth, paddedHeight] as [number, number];
};
