import React, { LC, Provide } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture, Scissor,
  OrbitCamera, OrbitControls, Cursor,
  ArrowLayer, LineLayer, PointLayer, RawData, CompositeData, LineSegments, ArrowSegments,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Flex, Block, Embed,
} from '@use-gpu/layout';
import {
  Cartesian, Axis, Grid, Sampled, Line, Point, Embedded, Surface, RangeContext,
} from '@use-gpu/plot';

import { Rotate } from './Rotate';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const DATA = [
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];

const π = Math.PI;

const W = DATA[0].length;
const H = DATA.length;
const PAD = 10;
const WP = W;
const HP = H;

const SLOPE = 1/3.38;

const lerp = (a: number, b: number, t: number) => a * (1 - t) + b * t;
const clamp = (x: number, a: number, b: number) => Math.max(a, Math.min(b, x));

for (let x = 0; x < W; ++x) {
  const yl = x * SLOPE + 1;
  const yr = (x + 1) * SLOPE + 1;

  for (let y = 0; y < yl; ++y) {
    DATA[y][x] = 1;
  }

  const yfl = Math.floor(yl);
  const yfr = Math.floor(yr);

  const dl = yl - yfl;
  const dr = yr - yfr;

  if (yfl === yfr) {
    DATA[yfl][x] = (dl + dr) / 2;
  }
  else {
    const t = (yfr - yl) / (yr - yl);
    DATA[yfl][x] = 1 - (t * (1 - dl) / 2);
    DATA[yfr][x] = ((1 - t) * dr / 2);
  }
}

const INNER = DATA.map(r => r.map(x => x < 1 ? 255 : 0));
const OUTER = DATA.map(r => r.map(x => x > 0 ? 255 : 0));

const toMask = (xs: number[]) => {
  const n = xs.length;
  const out = new Uint32Array(n);
  for (let i = 0; i < n; ++i) {
    const x = xs[i];
    let v = Math.round(x * 255);
    out[i] = v | (v<<8) | (v<<16) | 0xFF000000;
  }
  return out;
}

const SAMPLES = [
  [3, 1], [3, 2],
  [6, 2], [6, 3],
  [10, 3], [10, 4],
];

const samples = SAMPLES.map(([x, y]) => [
  [x, 0, y],
  [x + 1, 0, y],
  [x + 1, 0, y + 1],
  [x, 0, y + 1],
]);

const TEXTURE = {
  data: toMask(DATA.flatMap(x => x)),
  size: [W, H],
  format: 'rgba8unorm',
  colorSpace: 'linear',
};

const dataFields = [
  ['array<vec3<f32>>', o => o]
];

const cutoff = 0.25;
const decode = (x: number) => Math.max(0, (1 - cutoff - x / 255) * PAD);

export const VoronoiSlope3D: LC = () => {
  useResize();

  const w = window.innerWidth;
  const h = window.innerHeight;

  const points = [];
  DATA.forEach((row, j) => {
    row.forEach((x, i) => {
      if (x > 0 && x < 1) points.push([i + .5 + (j*.001), 0, j + .5 + (i*.001)]);
    });
  });

  const innerImage = new Uint8Array(toMask(INNER.flatMap(x => x)));
  const outerImage = new Uint8Array(toMask(OUTER.flatMap(x => x)));

  const innerSDF = glyphToSDF(innerImage, W, H, 0, PAD, undefined, false, false, false).data;
  const outerSDF = glyphToSDF(outerImage, W, H, 0, PAD, undefined, false, false, false).data;

  const sdf = new Float32Array(W * H);
  for (let i = 0; i < W * H; ++i) {
    sdf[i] = Math.max(0, decode(innerSDF[i * 4]) - 0.5)*1 - 1*Math.max(0, decode(outerSDF[i * 4]) - 0.5);
  }

  const ease = x => x*x*(3 - 2*x);

  return (
    <LinearRGB>
      <Camera>
        <Pass>
          <Cartesian
            range={[[0, W], [-W/2, W/2], [0, H]]}
            scale={[1, 1, H/W]}
          >
            <Grid
              axes="xz"
              color='#666'
              zBias={-1}
              first={{end: true, divide: W}}
              second={{end: true, divide: H}}
              zBias={1}
            />

            <Scissor range={[[0, W], [0, H]]}>
              <RawData
                length={points.length}
                data={points.flatMap(x => x)}
                format={'vec3<f32>'}
                render={(positions) =>
                  <PointLayer
                    positions={positions}
                    size={6}
                    color={[1, 1, 1, 1]}
                    zBias={1}
                    depth={0.75}
                  />
                }
              />

            </Scissor>

            <Provide context={RangeContext} value={[[0.5, W-.5], [-1, 1], [0.5, H-.5]]}>
              <Sampled
                axes="xz"
                format="vec3<f32>"
                size={[(W-1)*4+1, (H-1)*4+1]}
                index
                expr={(emit, x, z, i, j) => {
                  x -= .5;
                  z -= .5;

                  const ix = Math.min(W-2, Math.floor(x));
                  const iz = Math.min(H-2, Math.floor(z));

                  let dx = ease(ease(ease(x - ix)));
                  let dz = ease(ease(ease(z - iz)));

                  const base = (ix + iz * WP);

                  const d00 = sdf[base];
                  const d10 = sdf[base + 1];
                  const d01 = sdf[base + WP];
                  const d11 = sdf[base + (WP + 1)];

                  const d = lerp(lerp(d00, d10, dx), lerp(d01, d11, dx), dz);
                  emit(ix + dx + .5, d, iz + dz + .5);
                }}
              >
                <Surface
                  color={"#37f"}
                />
              </Sampled>
            </Provide>

            <CompositeData
              data={samples}
              fields={dataFields}
              on={<LineSegments />}
              loop={() => true}
              render={(positions, segments) =>
                <LineLayer
                  positions={positions}
                  segments={segments}
                  width={5}
                  color={[1, 1, 1, .1]}
                  zBias={1}
                  depthTest={false}
                  depthWrite={false}
                  mode={'transparent'}
                />
              }
            />

            <CompositeData
              data={samples}
              fields={dataFields}
              on={<LineSegments />}
              loop={() => true}
              render={(positions, segments) =>
                <LineLayer
                  positions={positions}
                  segments={segments}
                  width={3}
                  color={[1, 1, 1, 1]}
                  zBias={1}
                />
              }
            />

            <Sampled
              axes="xz"
              size={[3, 3]}
              format="vec3<f32>"
              index
              expr={(emit, x, z) => {
                emit(x, -1e-3, z)
              }}
            >
              <Surface
                color={[0, 0, 0, .65]}
                mode="transparent"
                zBias={-1}
              />
            </Sampled>
          </Cartesian>
        </Pass>
      </Camera>
      <Rotate />
    </LinearRGB>
  );
};

const Camera = ({children}: PropsWithChildren<object>) => (
  <OrbitControls
    radius={2.2}
    bearing={0.6}
    pitch={0.4}
    minPitch={-0.15}
    maxPitch={1.5}
    minRadius={0.1}
    maxRadius={5}
    render={(radius: number, phi: number, theta: number, target: vec3) =>
      <OrbitCamera
        fov={40 * π / 180}
        far={10000}
        radius={radius}
        phi={phi}
        theta={theta}
        target={target}
      >
        <Cursor cursor="move" />
        {children}
      </OrbitCamera>
    }
  />
);
