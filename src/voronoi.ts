type XY = [number, number];
type Edge = {
  start: XY,
  end: XY,
  left: XY,
  right: XY,
};

type Span = {
  min: number,
  max: number,
  lines: [XY, XY, XY, XY][],
};

const seq = (n: number, s: number = 0, d: number = 1): number[] => Array.from({ length: n }).map((_, i: number) => s + d * i);
const sqr = (x: number) => x * x;
const clone = (x: any) => JSON.parse(JSON.stringify(x));

let debugged = false;

export const voronoiSweep = (
  width: number,
  height: number,
  points: XY[],
  debug?: (state: any) => void,
) => {
  if (!points.length) throw new Error("empty voronoi");

  // Generate DF via wavefront
  const field = new Float32Array(width * height * 4);
  const wavefront = seq(width).map(_ => 0);

  // Sort points in Y->X order
  const sorted = points.sort((a, b) => (a[1] - b[1]) || (a[0] - b[0]));

  const queue: Arc[] = [];
  const arcs: Arc[] = [];
  const edges: Edge[] = [];

  const ySet = new Set<number>();
  const mesh: [XY, XY][] = [];
  const pairs: [XY, XY][] = [];

  const insertArc = (arc: Arc) => {
    if (!arcs.length) {
      arcs.push(arc);
      return [null, null, [null, null]];
    }

    const {point} = arc;
    const indices = findArc(arcs, point[0], point[1]);

    const [l, r] = indices;
    const left = arcs[l];
    removeEvent(left);

    const first = {...left, right: null};
    let second: Arc | null = null;

    if (r != null) {
      const right = arcs[r];
      removeEvent(right);

      second = {...right, left: null};
      arcs.splice(l, 1, first, arc, second);
    }
    else {
      arcs.splice(l, 1, first, arc);
    }

    return [first, second, indices];
  };

  const insertEvent = (arc: Arc, point: XY, bottom: number, index: number) => {
    if (arc.event) throw new Error("two events for same arc");

    arc.event = point;
    arc.y = bottom;

    const left = arcs[index - 1];
    const right = arcs[index + 1];
    if (left) left.right = arc;
    if (right) right.left = arc;

    const i = queue.findIndex(({y}) => bottom < y);
    if (i === -1) queue.push(arc);
    else queue.splice(i, 0, arc);
    setNextEvent();
  };

  const removeEvent = (arc: Arc, pending: boolean = true) => {
    if (!arc.event) return;

    arc.event = null;

    const index = arcs.indexOf(arc);
    const left = arcs[index - 1];
    const right = arcs[index + 1];
    if (left) left.right = null;
    if (right) right.left = null;

    if (!pending) return;

    const i = queue.indexOf(arc);
    queue.splice(i, 1);
    setNextEvent();
  };

  const checkConvergence = (index: number, y: number) => {
    if (index <= 0 || index >= arcs.length - 1) return;

    const left = arcs[index - 1].point;
    const center = arcs[index].point;
    const right = arcs[index + 1].point;

    const [vertex, bottom] = getConvergence(left, center, right);
    if (vertex && bottom > y) insertEvent(arcs[index], vertex, bottom, index);
  };

  let lastPoint: XY | null = null;
  const handlePoint = (point: XY) => {
    const [x, y] = point;

    if (lastPoint && lastPoint[0] === x && lastPoint[1] === y) return;
    lastPoint = point;

    const arc = {point, y};
    const [left, right, [l, r]] = insertArc(arc);

    if (l == null && r == null) return;

    if (l != null && r == null) {
      const p = [(point[0] + left.point[0]) / 2, 0];

      const edge = {start: p, end: p, left: left.point, right: point};
      edges.push(edge);
    }
    else {
      const p = [point[0], sampleArc(left.point, point[0], point[1])];

      if (l !== r) {
        let left = edges[l];
        finalizeEdge(left.start, p, left.point, right.point);
        edges.splice(l, 1);
      }
      const a = {start: p, end: p, left: left.point, right: point};
      const b = {start: p, end: p, left: right.point, right: point};
      edges.splice(l, 0, a, b);

      checkConvergence(l, y);
      checkConvergence(l + 2, y);
    }
  };

  const handleEvent = (arc: Arc) => {
    const {event, left, right, y} = arc;

    if (left) removeEvent(left);
    if (right) removeEvent(right);
    removeEvent(arc, false);

    const index = arcs.indexOf(arc);
    arcs.splice(index, 1);

    {
      const left = edges[index - 1];
      const right = edges[index];

      if (!left && !right) return;
      if (!left || !right) throw new Error("trying to collapse with one side missing");

      finalizeEdge(left.start, event, left.left, left.right);
      finalizeEdge(right.start, event, right.left, right.right);

      const edge = {start: event, end: event, left: arcs[index - 1]?.point, right: arcs[index]?.point};
      edges.splice(index - 1, 2, edge);
    }

    checkConvergence(index - 1, y);
    checkConvergence(index, y);
  };

  const finalizeEdge = (start: XY, end: XY, left: XY, right: XY) => {
    const flip = left && right && left[0] > right[0];

    mesh.push([start, end]);
    pairs.push(flip ? [right, left] : [left, right]);

    ySet.add(start[1]);
    ySet.add(end[1]);
  };

  const renderField = () => {
    const ys = Array.from(ySet.values());
    ys.sort((a, b) => a - b);
    if (ys.length === 0) return;

    const bins = ys.map(_ => []);
    bins.pop();

    let i = 0;
    for (const [s, e] of mesh) {
      const [l, r] = pairs[i++];

      const min = Math.min(s[1], e[1]);
      const max = Math.max(s[1], e[1]);
      if (min === max) continue;

      const a = Math.max(0, ys.findIndex(y => y > min));
      let b = ys.findIndex(y => y > max);
      if (b === -1) b = ys.length;

      const line = [s, e, l, r];
      for (let i = a; i < b; ++i) {
        bins[i - 1].push(line);
      }
    }

    let j = 0;
    for (const bin of bins) {
      const y = (ys[j] + ys[++j]) / 2;
      bin.sort((a, b) => {
        const ai = intersectLines(a[0], a[1], [0, y], [1, y]);
        const bi = intersectLines(b[0], b[1], [0, y], [1, y]);

        if (ai && bi) return ai[0] - bi[0];
        return +!!ai - +!!bi;
      });
    }

    let k = 0;
    for (let y = 0; y < height; ++y) {
      const yy = y + 0.5;
      while (yy >= ys[k]) k++;

      const lines = bins[k - 1];
      if (!lines) break;

      let l = 0;
      let line = lines[l];
      if (!line) break;

      let point = line[2];
      for (let x = 0; x < width; ++x) {
        const xx = x + 0.5;

        while (line) {
          let [xr] = intersectLines(line[0], line[1], [0, yy], [1, yy]);
          if (xx < xr) break;

          point = line[3];
          line = lines[++l];
        }

        const o = (x + y * width) * 4;
        const d = Math.sqrt(sqr(point[0] - xx) + sqr(point[1] - yy)) * 8;

        const i = (sorted.findIndex(p => p[0] === point[0] && p[1] === point[1]) % 7) + 1;

        field[o] = i & 1 ? d : 0;
        field[o+1] = i & 2 ? d : 0;
        field[o+2] = i & 4 ? d : 0;
        field[o+3] = 255;
      }
    }
  };

  const connectEnds = () => {
    let n = edges.length;
    for (let i = 0; i < n; ++i) {
      const edge = edges[i];

      const [lx, ly] = arcs[i].point;
      const [rx, ry] = arcs[i + 1].point;

      const {start, left, right} = edge;

      const dx = lx - rx;
      const dy = ly - ry;

      let end: XY | null = [
        start[0] + dy,
        start[1] - dx,
      ];

      if (start[0] < 0 || start[1] < 0 || start[0] > width || start[1] > height) {
        end = null;
      }
      else if (ly === ry) {
        const sy = lx < rx ? height : 0;
        end = intersectLines(start, end, [0, sy], [width, sy]);
      }
      else if (lx === rx) {
        const sx = ly > ry ? width : 0;
        end = intersectLines(start, end, [sx, 0], [sx, height]);
      }
      else {
        const m = dy / dx;
        if (Math.abs(m) < 1) {
          const sy = lx < rx ? height : 0;
          end = intersectLines(start, end, [0, sy], [width, sy]);
        }
        else {
          const sx = ly > ry ? width : 0;
          end = intersectLines(start, end, [sx, 0], [sx, height]);
        }
      }
      if (end) {
        if (end[1] < 0) end = intersectLines(start, end, [0, 0], [width, 0]);
        if (end[1] > height) end = intersectLines(start, end, [0, height], [width, height]);
        finalizeEdge(start, end, left, right);
      }
    }
  };

  const debugState = (y: number) => {
    if (!debug) return;

    y += 0.001;
    const line: XY[] = [[0, y], [width, y]];
    const sweeps: XY[][] = debugSweep(y);
    const breaks: XY[][] = debugBreaks(y);
    const events: XY[] = debugEvents();
    const circles: XY[][] = debugCircles();

    const state = {mesh: clone(mesh), sweeps, line, circles, breaks, events};
    debug(state);
  };

  const debugEvents = () => {
    return queue.map((arc) => ({point: arc.point, index: arcs.indexOf(arc)}));
  };

  const debugBreaks = (y: number) => {
    const out = [];

    arcs.slice(0, -1).map((arc, i) => {
      const a = arc.point;
      const b = arcs[i + 1].point;
      const x = getRightSide(a, b, y);
      out.push([x, sampleArc(a, x, y)]);
    });

    return out.slice(0, -1).map((a, i) => [a, out[i + 1]]);
  };

  const debugCircles = () => {
    const paths = [];
    for (const {event, y: bottom} of queue) {
      const path = [];
      const r = event[1] - bottom;
      const [x, y] = event;
      for (let i = 0; i <= 64; ++i) {
        const a = i/64 * Math.PI*2;
        path.push(x + r * Math.cos(a), y + r * Math.sin(a));
      }
      paths.push(path);
    }
    return paths;
  };

  const debugArc = (y: number, arc?: Arc | null) => {
    const line = [];
    if (!arc) return line;
    for (let i = 0; i < width; i += 0.25) {
      line.push([i, sampleArc(arc.point, i, y)]);
    }
    return line;
  };

  const debugSweep = (y: number) => {
    const line = [];
    const STEP = 1;
    y += 1e-6;

    let x = 0;
    arcs.forEach((arc, i) => {
      const left = arcs[i - 1]?.point;
      const center = arc.point;
      const right = arcs[i + 1]?.point;

      const xl = getLeftSide(center, left, y);
      const xr = getRightSide(center, right, y);

      /*
      if (Math.abs(xr - xl) < STEP) {
        if (xl !== xr) {
          line.push([x, sampleArc(center, x, y)]);

          x = arc.point[0];
          line.push([x, y]);
        }
      }
      else {
      */
        while (x <= xr && x < width) {
          line.push([x, sampleArc(center, x, y)]);
          x += STEP;
        }
      //}
      x = xr;
    });

    return line.length ? [line] : [];
  }

  let line = 0;
  let nextEvent: XY | null = null;
  const setNextEvent = () => {
    const [arc] = queue;
    nextEvent = arc ? [arc.point[0], arc.y] : null;
  };

  let i = 0;
  const n = sorted.length;
  while (i < n) {
    const point = sorted[i];
    const [x, y] = point;

    if (nextEvent && (nextEvent[1] <= y || (nextEvent[1] == y && nextEvent[0] <= x))) {
      const [arc] = queue;
      queue.shift();
      setNextEvent();

      handleEvent(arc);
      debugState(arc.y);
    }
    else {
      handlePoint(point);
      debugState(y);
      ++i;
    }
  }

  try {
    while (queue.length) {
      const arc = queue.shift();
      handleEvent(arc);
      debugState(arc.y);
    }
  }
  catch (e){
    console.warn(e);
  }

  connectEnds();
  renderField();
  debugState(height);

  return {mesh, field};
};

type Arc = {
  point: XY,
  event?: XY,
  y: number,

  left?: Arc | null,
  right?: Arc | null,
};

const makeArcList = () => {

  const head: Arc | null = null;

  const insert = (arc: Arc) => {
    if (!head) {
      head = arc;

      arc.left = null;
      arc.right = null;
      return;
    }
  };

};

const sampleArc = (point: XY, x: number, y: number) => {
  const [px, py] = point;
  return (sqr(x) - sqr(y) + sqr(px) + sqr(py) - 2 * px * x) / (2 * (py - y));
};

const findArc = (arcs: Arc[], x: number, y: number): number => {
  if (arcs.length === 0) return [null, null];

  const EPS = 1e-9;

  const n = arcs.length - 1;

  let start = 0;
  let end = n;

  do {
    let mid = start + Math.floor((end - start) / 2);

    const a = arcs[mid - 1];
    const b = arcs[mid];
    const c = arcs[mid + 1];

    const xl = getLeftSide(b.point, a?.point, y);
    const xr = getRightSide(b.point, c?.point, y);

    const dxl = x - xl;
    const dxr = x - xr;

    if (dxl < -EPS) {
      if (mid === end) return [mid, mid];
      end = mid;
    }
    else if (dxr >  EPS) {
      start = mid + 1;
    }
    else {
      if (dxl < EPS) return [mid - 1, mid];
      else if (dxr > -EPS) return [mid, mid + 1];
      else return [mid, mid];
    }
  } while (end >= start);

  return [end, null];
};

const getConvergence = (left: XY, center: XY, right: XY): [XY | null, number | null] => {
  const lx = (left[0] + center[0]) / 2
  const ly = (left[1] + center[1]) / 2;

  const rx = (right[0] + center[0]) / 2
  const ry = (right[1] + center[1]) / 2;

  const ldx = -(left[1] - center[1]) / 2;
  const ldy = (left[0] - center[0]) / 2;

  const rdx = -(right[1] - center[1]) / 2;
  const rdy = (right[0] - center[0]) / 2;

  const p = intersectRays(lx, ly, ldx, ldy, rx, ry, rdx, rdy);
  if (!p) return [null, null];

  const l = Math.sqrt(sqr(center[0] - p[0]) + sqr(center[1] - p[1]));
  return [p, p[1] + l];
};

export const intersectRays = (
  ax: number, ay: number,
  adx: number, ady: number,

  bx: number, by: number,
  bdx: number, bdy: number,
): XY | null => {
  const c1 = ady * ax - adx * ay;
  const c2 = bdy * bx - bdx * by;

  const det = bdy * adx - ady * bdx;
  if (Math.abs(det) < 1e-5) return null;
  if (det > 0) return null; // ignore clockwise

  const x = (adx * c2 - bdx * c1) / det;
  const y = (ady * c2 - bdy * c1) / det;
  return [x, y];
};

export const getLeftSide = (
  center: XY,
  left: XY | null,
  query: number,
) => {
  const y = query;
  const [qx, qy] = center;

  if (qy === y) return qx;
  if (!left) return -Infinity;

  const [px, py] = left;
  if (py === y) return px;

  const pq = (py - y) / (qy - y);
  if (pq === 1) return (px + qx) / 2;

  const a = 1 - pq;
  const b = -2 * px + 2 * qx * pq;
  const c = sqr(px) + sqr(py) - sqr(y) - (sqr(qx) + sqr(qy) - sqr(y)) * pq;

  const d = Math.sqrt(b*b - 4*a*c);
  return (-b + d) / (2 * a);
};

export const getRightSide = (
  center: XY,
  right: XY | null,
  query: number,
) => {
  const y = query;
  const [qx, qy] = center;

  if (qy === y) return qx;
  if (!right) return Infinity;

  return getLeftSide(right, center, query);
};

export const getRowIntersection = (left: XY, right: XY, y: number) => {
  const ax = (left[0] + right[0]) / 2
  const ay = (left[1] + right[1]) / 2;

  const adx = -(left[1] - right[1]) / 2;
  const ady = (left[0] - right[0]) / 2;

  const c1 = ady * ax - adx * ay;
  const c2 = -y;

  const det = -ady;
  if (Math.abs(det) < 1e-5) return null;

  return (adx * c2 - c1) / det;
};

export const getSideOf = (left: XY, right: XY, x: number, y: number) => {
  const lx = left[0] - x;
  const ly = left[1] - y;
  const rx = right[0] - x;
  const ry = right[1] - y;

  return (sqr(lx) + sqr(ly) - sqr(rx) - sqr(ry));
};

export const intersectLines = (a: XY, b: XY, c: XY, d: XY) => {
  const a1 = b[1] - a[1];
  const b1 = a[0] - b[0];
  const c1 = a1 * a[0] + b1 * a[1];

  const a2 = d[1] - c[1];
  const b2 = c[0] - d[0];
  const c2 = a2 * c[0] + b2 * c[1];

  const det = a1 * b2 - a2 * b1;
  if (Math.abs(det) < 1e-5) return null;

  const x = (b2 * c1 - b1 * c2) / det;
  const y = (a1 * c2 - a2 * c1) / det;
  const p = [x, y] as XY;

  return p;
};

export const getDistanceToLine = (a: XY, b: XY, p: XY) => {
  const dx = b[0] - a[0];
  const dy = b[1] - a[1];
  const px = p[0] - a[0];
  const py = p[1] - a[1];

  const len = Math.sqrt(dx * dx + dy * dy);
  return len ? (px * dy - py * dx) / len : 0;
};
