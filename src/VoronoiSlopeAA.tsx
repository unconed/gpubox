import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture, Scissor,
  ArrowLayer, LineLayer, PointLayer, RawData, CompositeData, LineSegments, ArrowSegments,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Flex, Block, Embed,
} from '@use-gpu/layout';
import {
  Cartesian, Axis, Grid, Sampled, Line, Point, Embedded,
} from '@use-gpu/plot';

import { Label } from './Label';
import { voronoiSweep } from './voronoi';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const DATA = [
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];

const W = DATA[0].length;
const H = DATA.length;

const slope = 1/3.38;
for (let x = 0; x < W; ++x) {
  const yl = x * slope + 1;
  const yr = (x + 1) * slope + 1;

  for (let y = 0; y < yl; ++y) {
    DATA[y][x] = 1;
  }

  const yfl = Math.floor(yl);
  const yfr = Math.floor(yr);

  const dl = yl - yfl;
  const dr = yr - yfr;

  if (yfl === yfr) {
    DATA[yfl][x] = (dl + dr) / 2;
  }
  else {
    const t = (yfr - yl) / (yr - yl);
    DATA[yfl][x] = 1 - (t * (1 - dl) / 2);
    DATA[yfr][x] = ((1 - t) * dr / 2);
  }
}

const toMask = (xs: number[]) => {
  const n = xs.length;
  const out = new Uint32Array(n);
  for (let i = 0; i < n; ++i) {
    const x = xs[i];
    let v = Math.round(x * 255);
    out[i] = v | (v<<8) | (v<<16) | 0x9F000000;
  }
  return out;
}

const TEXTURE = {
  data: toMask(DATA.flatMap(x => x)),
  size: [W, H],
  format: 'rgba8unorm',
  colorSpace: 'linear',
};

const dataFields = [
  ['array<vec2<f32>>', o => o]
];

export const VoronoiSlopeAA: LC = () => {
  useResize();

  const w = window.innerWidth;
  const h = window.innerHeight;

  const points = [];
  DATA.forEach((row, j) => {
    row.forEach((x, i) => {
      if (x) points.push([i + .5 + (j*.001), j + .5 + (i*.001)]);
    });
  });

  return (
    <LinearRGB>
      <Flat>
        <Pass>

          <UI>
            <Layout>
              <Flex width={"100%"} height={"100%"} align="center">
                <RawTexture data={TEXTURE} render={(texture =>
                  <Block padding={10} width={"100%"}>
                    <Block
                      width={"100%"}
                      aspect={W / H}
                      image={{
                        texture,
                        fit: 'scale',
                      }}
                    >
                      <Embed width="100%" height="100%">
                        <Embedded normalize>
                          <Cartesian
                            range={[[0, W], [0, H]]}
                          >
                            <Grid
                              axes="xy"
                              color='#666'
                              zBias={-1}
                              first={{end: true, divide: W}}
                              second={{end: true, divide: H}}
                              zBias={1}
                            />

                            <Scissor range={[[0, W], [0, H]]}>
                              <RawData
                                length={points.length}
                                data={points.flatMap(x => x)}
                                format={'vec2<f32>'}
                                render={(positions) =>
                                  <PointLayer
                                    positions={positions}
                                    size={16}
                                    color={[.5, .5, .5, 1]}
                                    zBias={2}
                                  />
                                }
                              />

                            </Scissor>
                          </Cartesian>
                        </Embedded>
                      </Embed>
                    </Block>
                  </Block>
                )}/>
              </Flex>
            </Layout>
          </UI>
        </Pass>
      </Flat>
    </LinearRGB>
  );
};

