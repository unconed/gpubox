import React, { LC, useOne } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture, TextureShader,
  useDeviceContext, useFontContext, DebugProvider,
  Loop, Animate, Keyframe,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex,
} from '@use-gpu/layout';
import { wgsl } from '@use-gpu/shader/wgsl';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const LINEAR_SAMPLER = {
  minFilter: 'linear',
  magFilter: 'linear',
};

const contrastShader = wgsl`
@link fn getTexture(uv: vec2<f32>) -> vec4<f32>;
@link fn getTextureSize() -> vec2<f32>;
@link fn getContrast() -> f32;

fn main(uv: vec2<f32>) -> vec4<f32> {
  let size = getTextureSize();
  var uvt = uv;

  if (uv.x > 0.5) {
    let s = getContrast() * size.y / 15.0;
    uvt = round((uv - 0.5) * s) / s + 0.5;
  }
  let level = clamp(((getTexture(uvt).r - 0.75) * getContrast()) + 0.5, 0.0, 1.0);

  if (abs(uv.x - 0.5) < 0.5 / size.x) {
    return vec4<f32>(1.0, 1.0, 1.0, 1.0);
  }

  return vec4<f32>(vec3<f32>(pow(level, 2.2)), 1.0);
}
`;

const KEYFRAMES: KeyFrame<any>[] = [
  [0, 0.5],
  [5, 3.5],
  [10, 0.5],
];

const BLACK = [0.0, 0.0, 0.0, 1.0];

export const GlyphContrast: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  const [size] = useResize();

  const glyph = '@';
  const [glyphId] = rustText.findGlyph(0, glyph);
  const glyphMetrics = rustText.measureGlyph(0, glyphId ?? 5, DETAIL * 1.5);

  const {width, height, image} = glyphMetrics;
  const padded = getPaddedSize(width, height, RADIUS);
  const [paddedWidth, paddedHeight] = padded;

  const scale = size / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = true;
  const contours = false;
  const preprocess = true;
  const postprocess = false;

  const sdfData = glyphToSDF(image, width, height, RADIUS, RADIUS, undefined, subpixel, true, preprocess, postprocess).data;

  const sdfTexture = {
    data: sdfData,
    format: "rgba8unorm" as GPUTextureFormat,
    colorSpace: 'linear',
    size: padded,
  } as DataTexture;

  const contrast = 2;

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <Loop>
        <LinearRGB>
          <Flat>
            <Pass>
              <UI>
                <Layout>
                  <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                    <RawTexture
                      data={sdfTexture}
                      sampler={LINEAR_SAMPLER}
                      render={(texture) =>
                        <Animate
                          keyframes={KEYFRAMES}
                          prop={'level'}
                          render={(level) =>
                            <TextureShader
                              texture={texture}
                              shader={contrastShader}
                              args={[Math.pow(2, level)]}
                              render={(texture) => {
                                const image = useOne(() => ({
                                    texture,
                                    fit: 'scale',
                                    repeat: 'none',
                                }), texture);
                                return (
                                  <Block width={w}>
                                    <Block width={w} height={h} fill={BLACK} image={image} />
                                  </Block>
                                );
                              }
                            }/>
                        }/>
                    }/>

                  </Flex>
                </Layout>
              </UI>
            </Pass>
          </Flat>
        </LinearRGB>
      </Loop>

    </DebugProvider>
  );
};

