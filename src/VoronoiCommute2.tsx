import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture, Scissor,
  ArrowLayer, LineLayer, PointLayer, RawData, CompositeData, LineSegments, ArrowSegments,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Flex, Block, Embed,
} from '@use-gpu/layout';
import {
  Cartesian, Axis, Grid, Sampled, Line, Point, Embedded,
} from '@use-gpu/plot';

import { Label } from './Label';
import { voronoiSweep } from './voronoi';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const DATA = [
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
];

const SAMPLES = [
  [[1, 2], [4, 2]],
  [[4, 2], [4, 8]],
  [[1, 2], [1, 8]],
  [[1, 8], [4, 8]],
  [[7, 1], [7, 8]],
  [[7, 8], [8, 8]],
  [[7, 8], [6, 8]],
];

const samples = SAMPLES.map(row => row.map(p => p.map(x => x + .5)));

const W = DATA[0].length;
const H = DATA.length;

const toMask = (xs: number[]) => {
  const n = xs.length;
  const out = new Uint32Array(n);
  for (let i = 0; i < n; ++i) {
    const x = xs[i];
    const v = Math.round(x * 255);
    out[i] = v | (v<<8) | (v<<16) | 0x9F000000;
  }
  return out;
}

const points = [];
DATA.forEach((row, j) => {
  row.forEach((x, i) => {
    if (x) {
      let d = 0;
      const t = (i - 6) / 6;
      if (j === 1 && i > 1 && i < 11) {
        d = t * t * 1.76 - 0.45;
      }
      else if (j === 2 && (i <= 1 || i >= 11)) {
        d = t * t * 1.75 - 1.45;
      }
      if (d) DATA[j][i] = 0.5;
      points.push([i + .5 + (j*.001), j + .5 + (i*.001) + d]);
    }
  });
});

const TEXTURE = {
  data: toMask(DATA.flatMap(x => x)),
  size: [W, H],
  format: 'rgba8unorm',
  colorSpace: 'linear',
};

const dataFields = [
  ['array<vec2<f32>>', o => o]
];

export const VoronoiCommute2: LC = () => {
  useResize();

  const w = window.innerWidth;
  const h = window.innerHeight;

  const voronoi = voronoiSweep(W, H, points);

  return (
    <LinearRGB>
      <Flat>
        <Pass>

          <UI>
            <Layout>
              <Flex width={"100%"} height={"100%"} align="center">
                <RawTexture data={TEXTURE} render={(texture =>
                  <Block padding={10} width={"100%"}>
                    <Block
                      width={"100%"}
                      aspect={W / H}
                      image={{
                        texture,
                        fit: 'scale',
                      }}
                    >
                      <Embed width="100%" height="100%">
                        <Embedded normalize>
                          <Cartesian
                            range={[[0, W], [0, H]]}
                          >
                            <Grid
                              axes="xy"
                              color='#666'
                              zBias={-1}
                              first={{end: true, divide: W}}
                              second={{end: true, divide: H}}
                              zBias={1}
                            />

                            <RawData
                              length={points.length}
                              data={points.flatMap(x => x)}
                              format={'vec2<f32>'}
                              render={(positions) =>
                                <PointLayer
                                  positions={positions}
                                  size={16}
                                  color={[.5, .5, .5, 1]}
                                  zBias={2}
                                />
                              }
                            />
                            <CompositeData
                              data={voronoi.mesh}
                              fields={dataFields}
                              on={<LineSegments />}
                              render={(positions, segments) =>
                                <LineLayer
                                  positions={positions}
                                  segments={segments}
                                  width={8}
                                  color={[.5, .5, .5, 1]}
                                  zBias={5}
                                />
                              }
                            />
                            <CompositeData
                              data={samples}
                              fields={dataFields}
                              on={<ArrowSegments />}
                              end={() => true}
                              render={(positions, segments, anchors, trims) =>
                                <ArrowLayer
                                  positions={positions}
                                  segments={segments}
                                  anchors={anchors}
                                  trims={trims}
                                  width={5}
                                  color={[1, 1, 1, 1]}
                                  zBias={10}
                                />
                              }
                            />
                          </Cartesian>
                        </Embedded>
                      </Embed>
                    </Block>
                  </Block>
                )}/>
              </Flex>
            </Layout>
          </UI>
        </Pass>
      </Flat>
    </LinearRGB>
  );
};

