import React, { useEffect, useRef, useState } from 'react';
import { base } from './App';
import { HTML } from '@use-gpu/react';

export const FALLBACK_MESSAGE = (error: Error) => <>
  <div className="error-message">{error.toString()}</div>
  <div className="help-message">
    <p><b>To enable WebGPU:</b></p>
    <ul>
      <li><b>Chrome 113+</b> – Windows, MacOS, ChromeOS ✅</li>
      <li><b>Chrome</b> – Linux, Android - Dev version required<br />Turn on <code>#enable-unsafe-webgpu</code> in <code>chrome://flags</code></li>
      <li><b>Firefox</b> – Nightly version required<br />Turn on <code>dom.webgpu.enabled</code> in <code>about:config</code></li>
    </ul>
    <p>See <a href="https://caniuse.com/webgpu">CanIUse.com</a> for more info.</p>
  </div>
</>;

const F = () => {};

type ImageProps = {
  id: string,
  retina?: boolean,
  type?: string,
};

type VideoProps = {
  id: string,
  retina?: boolean,
  type?: string,
};

export const Image = (props: ImageProps) => {
  const {
    id,
    retina = false,
    type = 'png',
  } = props;

  const path = `${base}image/${id}`;
  const src = retina ? `${path}@1x.${type}` : `${path}.${type}`;
  const srcSet = retina ? `${path}@1x.${type} 1x, ${path}@2x.${type} 2x` : undefined;

  return (
    <HTML container={document.body} style={{position: 'absolute', inset: 0}}>
      <img
        style={{position: 'absolute', inset: 0, width: '100%', height: '100%'}}
        src={src}
        srcSet={srcSet}
      />
    </HTML>
  );
};

export const Video = (props: VideoProps) => (
  <HTML container={document.body} style={{position: 'absolute', inset: 0}}>
    <VideoPlayer {...props} />
  </HTML>
);

export const VideoPlayer = (props: VideoProps) => {
  const {
    id,
    retina = false,
    type = 'mp4',
  } = props;

  const [play, setPlay] = useState(false);
  const [hover, setHover] = useState(false);
  const [touch, setTouch] = useState(false);

  useEffect(() => {
    const touchOn = () => setTouch(true);
    document.addEventListener('touchstart', touchOn, false);
    return () => {
      document.removeEventListener('touchstart', touchOn, false);
    };
  })

  const ref = useRef<HTMLVideoElement>(null);

  const showButton = (hover && !touch) || !play;

  const togglePlay = () => {
    const {current: video} = ref;
    if (!video) return;

    if (video.paused) {
      video.play();
      setPlay(true);
    }
    else {
      video.pause();
      setPlay(false);
    }
  };

  let path = `${base}video/${id}`;
  if (retina) {
    path += window.devicePixelRatio > 1 ? '@2x' : '@1x';
  }

  return (<>
    <video
      loop
      ref={ref}
      playsInline
      preload="auto"
      onClick={togglePlay}
      style={{position: 'absolute', inset: 0, width: '100%', height: '100%'}}
    >
      <source src={`${path}.${type}`} type={`video/${type}`} />
    </video>
    <div
      onClick={togglePlay}
      style={{
        position: 'absolute',
        left: '50%',
        top: '50%',
        margin: '-50px 0 0 -50px',
        width: '100px',
        height: '100px',
        borderRadius: '100%',
        border: '2px solid #fff',
        boxShadow: '0 1px 10px rgba(0, 0, 0, .25)',
        background: 'rgba(80, 80, 80, .5)',
        cursor: 'pointer',
        opacity: showButton ? 1 : 0,
        transition: '.15s opacity ease-in-out',
      }}
      onMouseEnter={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      {play ? (<>
        <div style={{
          position: 'absolute',
          left: '50%',
          top: '50%',
          marginLeft: '5px',
          marginTop: '-15px',        
          width: '40px',
          height: '30px',
          borderLeft: '10px solid #fff',
        }} />
        <div style={{
          position: 'absolute',
          left: '50%',
          top: '50%',
          marginLeft: '-15px',
          marginTop: '-15px',        
          width: '40px',
          height: '30px',
          borderLeft: '10px solid #fff',
        }} />
      </>) : (
        <div style={{
          position: 'absolute',
          left: '50%',
          top: '50%',
          marginLeft: '-17px',
          marginTop: '-20px',        
          width: '40px',
          border: '20px solid transparent',
          borderRight: 0,
          borderLeft: '40px solid #fff',
        }} />
      )}
    </div>
  </>);
};