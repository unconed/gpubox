import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture, PBRMaterial,
  OrbitControls, OrbitCamera, Cursor, Scissor,
  useDeviceContext, useFontContext, DebugProvider,
  DirectionalLight, AmbientLight,
} from '@use-gpu/workbench';
import {
  Cartesian, Axis, Grid, Sampled, Line, Point, Surface,
} from '@use-gpu/plot';

import { Rotate } from './Rotate';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const π = Math.PI;
const τ = Math.PI * 2;

const RANGE = [[0, 14], [0, 10], [-5.5, 1.5]];
const SCISSOR = [[0, 14.001], [-1, 10], [-5.5, 1.5]];

const DATA = [0, 0, 1, 4, 9, 4, 4, 4, 1, 1, 4, 9, 4, 9, 9];

const sampleParabola = (x: number, i: number) => {
  const y = (i - x) * (i - x) + DATA[i];
  return y;
};

const sampleParabolas = (x: number) => {
  let y = Infinity;
  for (let i = 0, n = DATA.length; i < n; ++i) {
    const d = sampleParabola(x, i);
    if (Number.isNaN(d)) continue;
    y = Math.min(y, d);
  }
  return y;
};

const emitParabola2D = (emit: any, i: number, phi: number, r: number) => {
  let c = Math.cos(phi);
  let s = Math.sin(phi);
  r = Math.max(1e-5, r);

  const dx = i;
  const dz = -Math.sqrt(DATA[i]);

  const x = dx - s * r;
  const z = dz - c * r;
  const y = r*r;

  return emit(x, y, z);
};

export const Parabola1DXY: LC = () => {
  useResize();

  const w = window.innerWidth;
  const h = window.innerHeight;

  return (
    <LinearRGB tonemap="aces" gain={1}>
      <Camera>
        <Pass lights>
          <AmbientLight intensity={0.1} />
          <DirectionalLight
            position={[0.267, 3*0.267, 2*0.267]}
            color={[1, 1, 1, 1]}
            intensity={3.9}
          />
          <DirectionalLight
            position={[-0.267*2, -0.267, 3*0.267]}
            color={[1, 1, 1, 1]}
            intensity={.5}
          />

          <Cartesian
            range={RANGE}
            scale={[300, 150, 150]}
          >
            <Axis axis="x" />
            <Axis axis="y" />
            <Grid axes="xy" color='#444' zBias={-1} />
            <Grid axes="xz" color='#444' zBias={-1} first={{divide: 10}} second={{divide: 5}} />

            <Scissor range={SCISSOR}>
              <Sampled
                axis="xy"
                size={[14*8 + 1, DATA.length]}
                format="vec2<f32>"
                index
                expr={(emit, x, y, i, j) => {
                  emit(x, sampleParabola(x, j))
                }}
              >
                <Line
                  width={2}
                  size={10}
                  color="#888"
                />
              </Sampled>

              <Sampled
                axis="x"
                size={[281]}
                format="vec2<f32>"
                index
                expr={(emit, x) => {
                  emit(x, sampleParabolas(x))
                }}
              >
                <Line
                  width={4}
                  color="#6af"
                  zBias={1}
                />
              </Sampled>

              <Sampled
                axis="x"
                size={[141]}
                format="vec2<f32>"
                index
                expr={(emit, x) => {
                  emit(x, sampleParabola(x, 5))
                }}
              >
                <Line
                  width={4}
                  color="#fff"
                  zBias={2}
                />
              </Sampled>

              <Sampled
                axis="xyz"
                size={[64, 32]}
                format="vec3<f32>"
                index
                expr={(emit, x, y, i, j) => {
                  emitParabola2D(emit, 5, i / 64 * τ, j / 9.4);
                }}
              >
                <PBRMaterial>
                  <Surface
                    loopX
                    color="#6af"
                    alphaToCoverage
                  />
                </PBRMaterial>
              </Sampled>

              <Sampled
                axis="x"
                size={[14]}
                format="vec2<f32>"
                index
                expr={(emit, x, i) => {
                  emit(i, sampleParabola(i, i))
                }}
              >
                <Point
                  size={8}
                  color="#888"
                  zBias={0}
                />
              </Sampled>

              <Sampled
                axis="x"
                size={[14]}
                format="vec2<f32>"
                index
                expr={(emit, x, i) => {
                  emit(i, sampleParabolas(i))
                }}
              >
                <Point
                  size={8}
                  color="#fff"
                  zBias={4}
                />
              </Sampled>

              <Sampled
                axis="x"
                size={[1]}
                format="vec3<f32>"
                index
                expr={(emit, x, i) => {
                  emit(5, 0, -Math.sqrt(DATA[5]))
                }}
              >
                <Point
                  size={8}
                  color="#fff"
                  zBias={5}
                />
              </Sampled>

              <Sampled
                axes="xz"
                size={[3, 3]}
                format="vec3<f32>"
                index
                expr={(emit, x, z) => {
                  emit(x, 0, z)
                }}
              >
                <PBRMaterial>
                  <Surface
                    color={[0, 0, 0, .5]}
                    blend="alpha"
                    mode="transparent"
                    depthWrite={false}
                    zBias={-1}
                  />
                </PBRMaterial>
              </Sampled>

              <Sampled
                axes="xy"
                size={[3, 3]}
                format="vec3<f32>"
                index
                expr={(emit, x, y) => {
                  emit(x, y, 0)
                }}
              >
                <PBRMaterial>
                  <Surface
                    color={[0, 0, 0, .5]}
                    blend="alpha"
                    mode="transparent"
                    depthWrite={false}
                    zBias={-1}
                  />
                </PBRMaterial>
              </Sampled>

            </Scissor>
          </Cartesian>
        </Pass>
      </Camera>
      <Rotate />
    </LinearRGB>
  );
};


const Camera = ({children}: PropsWithChildren<object>) => (
  <OrbitControls
    radius={720}
    bearing={0.5}
    pitch={0.3}
    minPitch={-0.15}
    maxPitch={1.5}
    minRadius={100}
    maxRadius={850}
    render={(radius: number, phi: number, theta: number, target: vec3) =>
      <OrbitCamera
        fov={40 * π / 180}
        far={10000}
        radius={radius}
        phi={phi}
        theta={theta}
        target={target}
      >
        <Cursor cursor="move" />
        {children}
      </OrbitCamera>
    }
  />
);

