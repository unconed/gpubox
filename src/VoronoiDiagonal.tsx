import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture, Scissor,
  ArrowLayer, LineLayer, PointLayer, RawData, CompositeData, LineSegments, ArrowSegments,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Flex, Block, Embed,
} from '@use-gpu/layout';
import {
  Cartesian, Axis, Grid, Sampled, Line, Point, Embedded,
} from '@use-gpu/plot';

import { Label } from './Label';
import { voronoiSweep } from './voronoi';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const DATA = [
  [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
  [0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1],
  [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1],
  [0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
  [0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1],
  [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
];

const SAMPLES = [
  [[4, 3], [5, 2]],

  [[6, 4], [6, 3]],
  [[6, 4], [7, 4]],
];

const SAMPLES2 = [
  [[6, 4], [6.5, 3.5]],
];

const samples = SAMPLES.map(row => row.map(p => p.map(x => x + .5)));
const samples2 = SAMPLES2.map(row => row.map(p => p.map(x => x + .5)));

const W = DATA[0].length;
const H = DATA.length;

const toMask = (xs: number[]) => {
  const n = xs.length;
  const out = new Uint32Array(n);
  for (let i = 0; i < n; ++i) {
    const x = xs[i];
    const v = Math.round(x * 255);
    out[i] = v | (v<<8) | (v<<16) | 0x9F000000;
  }
  return out;
}

const TEXTURE = {
  data: toMask(DATA.flatMap(x => x)),
  size: [W, H],
  format: 'rgba8unorm',
  colorSpace: 'linear',
};

const dataFields = [
  ['array<vec2<f32>>', o => o]
];

export const VoronoiDiagonal: LC = () => {
  useResize();

  const w = window.innerWidth;
  const h = window.innerHeight;

  const points = [];
  DATA.forEach((row, j) => {
    row.forEach((x, i) => {
      if (x) points.push([i + .5 + (j*.001), j + .5 + (i*.001)]);
    });
  });

  const voronoi = voronoiSweep(W, H, points);

  return (
    <LinearRGB>
      <Flat>
        <Pass>

          <UI>
            <Layout>
              <Flex width={"100%"} height={"100%"} align="center">
                <RawTexture data={TEXTURE} render={(texture =>
                  <Block padding={10} width={"100%"}>
                    <Block
                      width={"100%"}
                      aspect={W / H}
                      image={{
                        texture,
                        fit: 'scale',
                      }}
                    >
                      <Embed width="100%" height="100%">
                        <Embedded normalize>
                          <Cartesian
                            range={[[0, W], [0, H]]}
                          >
                            <Grid
                              axes="xy"
                              color='#666'
                              zBias={-1}
                              first={{end: true, divide: W}}
                              second={{end: true, divide: H}}
                              zBias={1}
                            />

                            <Scissor range={[[0, W], [0, H]]}>
                              <RawData
                                length={points.length}
                                data={points.flatMap(x => x)}
                                format={'vec2<f32>'}
                                render={(positions) =>
                                  <PointLayer
                                    positions={positions}
                                    size={16}
                                    color={[.5, .5, .5, 1]}
                                    zBias={2}
                                  />
                                }
                              />
                              <CompositeData
                                data={voronoi.mesh}
                                fields={dataFields}
                                on={<LineSegments />}
                                render={(positions, segments) =>
                                  <LineLayer
                                    positions={positions}
                                    segments={segments}
                                    width={8}
                                    color={[.5, .5, .5, 1]}
                                    zBias={5}
                                  />
                                }
                              />
                              <CompositeData
                                data={samples}
                                fields={dataFields}
                                on={<ArrowSegments />}
                                end={() => true}
                                render={(positions, segments, anchors, trims) =>
                                  <ArrowLayer
                                    positions={positions}
                                    segments={segments}
                                    anchors={anchors}
                                    trims={trims}
                                    width={5}
                                    color={[1, 1, 1, 1]}
                                    zBias={10}
                                  />
                                }
                              />
                              <CompositeData
                                data={samples2}
                                fields={dataFields}
                                on={<ArrowSegments />}
                                end={() => true}
                                render={(positions, segments, anchors, trims) =>
                                  <ArrowLayer
                                    positions={positions}
                                    segments={segments}
                                    anchors={anchors}
                                    trims={trims}
                                    width={7}
                                    color={[.3, .9, 0, 1]}
                                    zBias={10}
                                  />
                                }
                              />
                            </Scissor>
                          </Cartesian>
                        </Embedded>
                      </Embed>
                    </Block>
                  </Block>
                )}/>
              </Flex>
            </Layout>
          </UI>
        </Pass>
      </Flat>
    </LinearRGB>
  );
};

