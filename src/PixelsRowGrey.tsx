import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Flex, Block, Embed,
} from '@use-gpu/layout';
import {
  Cartesian, Axis, Grid, Sampled, Line, Point, Embedded,
} from '@use-gpu/plot';

import { Label } from './Label';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const toMask = (xs: number[]) => {
  const n = xs.length;
  const out = new Uint32Array(n);
  for (let i = 0; i < n; ++i) {
    const x = xs[i];
    const v = Math.round(x * 255);
    out[i] = v | (v<<8) | (v<<16) | 0xFF000000;
  }
  return out;
}

const DATA = toMask([1, 1, 1, 0.25, 0, 0, 0, 0.15, 1, 0.5, 0, 0.25, 1, 1, 1]);
const TEXTURE = {
  data: DATA,
  size: [14, 1],
  format: 'rgba8unorm',
  colorSpace: 'linear',
};

export const PixelsRowGrey: LC = () => {
  useResize();

  const w = window.innerWidth;
  const h = window.innerHeight;

  return (
    <LinearRGB>
      <Flat>
        <Pass>
          <UI>
            <Layout>
              <Flex width={"100%"} height={"100%"} align="center">
                <RawTexture data={TEXTURE} render={(texture =>
                  <Block padding={0} width={"100%"}>
                    <Block
                      width={"100%"}
                      height={"100%"}
                      image={{
                        texture,
                        fit: 'scale',
                      }}
                    >
                      <Embed width="100%" height="100%">
                        <Embedded normalize>
                          <Cartesian
                            range={[[0, 14], [0, 1]]}
                          >
                            <Grid
                              axes="xy"
                              width={2}
                              color='#80808080'
                              mode="transparent"
                              zBias={-1}
                              first={{end: true}}
                              second={{divide: 1, end: true}} zBias={1}
                            />
                          </Cartesian>
                        </Embedded>
                      </Embed>
                    </Block>
                  </Block>
                )}/>
              </Flex>
            </Layout>
          </UI>
        </Pass>
      </Flat>
    </LinearRGB>
  );
};

