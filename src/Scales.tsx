import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex,
} from '@use-gpu/layout';

import { Label } from './Label';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const seq = (n: number, s: number = 0, d: number = 1): number[] => Array.from({ length: n }).map((_, i: number) => s + d * i);

export const Scales: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  useResize();

  const glyph = '@';
  const [glyphId] = rustText.findGlyph(0, glyph);
  const glyphMetrics = rustText.measureGlyph(0, glyphId ?? 5, DETAIL * 1.5);

  const {width, height, image} = glyphMetrics;
  const padded = getPaddedSize(width, height, RADIUS);
  const [paddedWidth, paddedHeight] = padded;

  const scale = window.innerWidth / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = true;
  const contours = false;
  const preprocess = false;
  const postprocess = false;

  const fontSize = DETAIL * scale * 1.5;

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <LinearRGB>
        <Flat>
          <Pass>
            <UI>
              <Layout>
                <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                  <Inline margin={10}>
                    {seq(21, 2, 1).map(s =>
                      <Text color="#fff" size={s} lineHeight={s * 1.5}>{"Grumpy wizards make toxic brew for the evil Queen and Jack\n"}</Text>
                    )}
                  </Inline>

                </Flex>
              </Layout>
            </UI>
          </Pass>
        </Flat>
      </LinearRGB>

    </DebugProvider>
  );
};

