import React, { LC } from '@use-gpu/live';

import { glyphToRGBA, glyphToSDF, makeSDFStage, paintSubpixelOffsets } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture, PanControls,
  CompositeData, LineLayer, LineSegments,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex, Embed,
} from '@use-gpu/layout';
import {
  Sampled, Grid, Axis, Cartesian, Embedded, Point, Arrow, Line, Scale, Tick,
} from '@use-gpu/plot';

import { Pannable } from './Pannable';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

const ZBIAS_GRID = 3;
const ZBIAS_DATA = 4;

export const ESDTOffsets: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  useResize();

  const glyph = '@';
  const [glyphId] = rustText.findGlyph(0, glyph);
  const glyphMetrics = rustText.measureGlyph(0, glyphId ?? 5, DETAIL * 1.5);

  const debugs: Image[] = [];
  const pushDebug = (image: Image) => debugs.push(image);

  const {width, height, image} = glyphMetrics;
  const radius = RADIUS;
  const padded = getPaddedSize(width, height, radius);
  const [paddedWidth, paddedHeight] = padded;

  const scale = window.innerWidth / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = true;
  const contours = false;
  const preprocess = false;
  const postprocess = true;

  const rgbaData = glyphToRGBA(image, width, height, radius).data;

  glyphToSDF(image, width, height, radius, radius, undefined, subpixel, true, preprocess, postprocess).data;

  const rgbaTexture = {
    data: rgbaData,
    format: "rgba8unorm" as GPUTextureFormat,
    colorSpace: 'srgb',
    size: padded,
  } as DataTexture;

  const s = Math.max(paddedWidth, paddedHeight);
  const sdf1 = makeSDFStage(s);
  const sdf2 = makeSDFStage(s);
  paintSubpixelOffsets(sdf1, image, width, height, radius, preprocess, true);
  paintSubpixelOffsets(sdf2, image, width, height, radius, preprocess, false);

  const {xo, yo, xi, yi} = sdf1;
  const {xo: xo2, yo: yo2, xi: xi2, yi: yi2} = sdf2;

  const outerField = {
    xs: xo,
    ys: yo,
    width: paddedWidth,
    height: paddedHeight,
  };

  const innerField = {
    xs: xi,
    ys: yi,
    width: paddedWidth,
    height: paddedHeight,
  };

  const outer2Field = {
    xs: xo2,
    ys: yo2,
    width: paddedWidth,
    height: paddedHeight,
  };

  const inner2Field = {
    xs: xi2,
    ys: yi2,
    width: paddedWidth,
    height: paddedHeight,
  };

  const gridEmitter = ({xs, ys, width, height}: DebugImage) =>
    (emit: Emit, x: number, y: number, i: number, j: number) => {
      const index = i + j * paddedWidth;
      const dx = xs[index];
      const dy = ys[index];
      if (dx || dy) emit(x, y, 0, 1);
    };

  const pointEmitter = ({xs, ys, width, height}: DebugImage) =>
    (emit: Emit, x: number, y: number, i: number, j: number) => {
      const index = i + j * paddedWidth;
      const dx = xs[index];
      const dy = ys[index];
      emit(x + dx, y + dy, 0, 1);
    };

  const arrowEmitter = ({xs, ys, width, height}: DebugImage) =>
    (emit: Emit, x: number, y: number, i: number, j: number) => {
      const index = i + j * paddedWidth;
      const dx = xs[index];
      const dy = ys[index];

      if (dx || dy) {
        emit(x, y, 0, 1);
        emit(x + dx, y + dy, 0, 1);
      }
    };

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <LinearRGB>
        <Pannable
          initialX={-24}
          initialY={26.35}
          initialZoom={scale * 24}
          minZoom={scale}
          maxZoom={scale * 32}
          focus={1/3}
        >
          <Pass>
            <UI>
              <Layout>
                <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                  <TextureFrame texture={rgbaTexture}>
                  {subpixel ? <>
                    <Sampled
                      axes='xy'
                      format='vec4<f32>'
                      size={padded}
                      items={1}
                      sparse
                      centered
                      index
                      expr={gridEmitter(outerField)}
                    >
                      <Point size={0.5} depth={1} color={'#808080'} shape={'circleOutlined'} zBias={ZBIAS_DATA} />
                    </Sampled>

                    <Sampled
                      axes='xy'
                      format='vec4<f32>'
                      size={padded}
                      items={1}
                      sparse
                      centered
                      index
                      expr={pointEmitter(outerField)}
                    >
                      <Point size={0.5} depth={1} color={preprocess ? '#80808080' : '#808080'} shape={'circle'} zBias={ZBIAS_DATA} />
                    </Sampled>

                    {preprocess ? <Sampled
                      axes='xy'
                      format='vec4<f32>'
                      size={padded}
                      items={1}
                      sparse
                      centered
                      index
                      expr={pointEmitter(innerField)}
                    >
                      <Point size={0.5} depth={1} color={'#808080'} shape={'circle'} zBias={ZBIAS_DATA + 1} />
                    </Sampled> : null}

                    <Sampled
                      axes='xy'
                      format='vec4<f32>'
                      size={padded}
                      items={2}
                      sparse
                      centered
                      index
                      expr={arrowEmitter(preprocess ? innerField : outerField)}
                    >
                      <Arrow width={3} depth={0.05} color={0x40c0ff} detail={4} zBias={ZBIAS_DATA} />
                    </Sampled>
                    </> : null}

                  </TextureFrame>

                </Flex>
              </Layout>
            </UI>
          </Pass>
        </Pannable>
      </LinearRGB>

    </DebugProvider>
  );
};

type TextureFrameProps = {
  texture: any,
  margin?: number,
}

const TextureFrame: LC<TextureFrameProps> = (props: PropsWithChildren<TextureFrameProps>) => {
  const {margin, texture, children} = props;
  const {size: [width, height]} = texture;

  return (
    <RawTexture data={texture} render={(texture) =>
      <Block margin={margin} width={width} height={height} fill={[0.0, 0.0, 0.0, 1.0]} image={{
        texture,
        fit: 'contain',
        repeat: 'none',
      }}>
        <Embed width="100%" height="100%">
          <Embedded>
            {children}
          </Embedded>
        </Embed>
      </Block>
    }/>
  );
}

