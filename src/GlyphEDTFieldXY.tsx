import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex,
} from '@use-gpu/layout';

import { Pannable } from './Pannable';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

export const GlyphEDTFieldXY: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  useResize();

  const glyph = '@';
  const [glyphId] = rustText.findGlyph(0, glyph);
  const glyphMetrics = rustText.measureGlyph(0, glyphId ?? 5, DETAIL * 1.5);

  const debugs: Image[] = [];
  const pushDebug = (image: Image) => debugs.push(image);

  const {width, height, image} = glyphMetrics;
  const padded = getPaddedSize(width, height, RADIUS);
  const [paddedWidth, paddedHeight] = padded;

  const scale = window.innerWidth / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = false;
  const contours = false;
  const preprocess = true;
  const postprocess = true;

  const radius = 30;

  glyphToSDF(image, width, height, radius, radius, undefined, subpixel, true, preprocess, postprocess, pushDebug).data;

  const sdfTexture = {
    data: debugs[2].data,
    format: "rgba8unorm" as GPUTextureFormat,
    colorSpace: 'srgb',
    size: [debugs[0].width, debugs[0].height],
  } as DataTexture;

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <LinearRGB>
        <Pannable>
          <Pass>
            <UI>
              <Layout>
                <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                  <RawTexture data={sdfTexture} render={(texture) =>
                    <Block width={w}>
                      <Block width={w} height={h} fill={[0.0, 0.0, 0.0, 1.0]} image={{
                        texture,
                        fit: 'scale',
                        repeat: 'none',
                      }} />
                    </Block>
                  }/>

                </Flex>
              </Layout>
            </UI>
          </Pass>
        </Pannable>
      </LinearRGB>

    </DebugProvider>
  );
};
