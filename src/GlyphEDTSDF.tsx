import React, { LC } from '@use-gpu/live';

import { glyphToSDF } from '@use-gpu/glyph';
import {
  LinearRGB, Pass, Flat, RawTexture,
  useDeviceContext, useFontContext, DebugProvider,
} from '@use-gpu/workbench';
import {
  UI, Layout, Block, Inline, Text, Flex, Absolute,
} from '@use-gpu/layout';

import { Label } from './Label';

import { SIZE, DETAIL, RADIUS } from './constants';
import { getPaddedSize } from './util';
import { useResize } from './hooks/useResize';

export const GlyphEDTSDF: LC = () => {

  const device = useDeviceContext();
  const rustText = useFontContext();
  useResize();

  const glyph = '@';
  const [glyphId] = rustText.findGlyph(0, glyph);
  const glyphMetrics = rustText.measureGlyph(0, glyphId ?? 5, DETAIL * 1.5);

  const {width, height, image} = glyphMetrics;
  const padded = getPaddedSize(width, height, RADIUS);
  const [paddedWidth, paddedHeight] = padded;

  const scale = window.innerWidth / paddedWidth;
  const w = paddedWidth * scale;
  const h = paddedHeight * scale;

  const subpixel = false;
  const contours = false;
  const preprocess = true;
  const postprocess = true;

  const sdfData = glyphToSDF(image, width, height, RADIUS, RADIUS, undefined, subpixel, true, preprocess, postprocess).data;

  const sdfTexture = {
    data: sdfData,
    format: "rgba8unorm" as GPUTextureFormat,
    colorSpace: 'srgb',
    size: padded,
  } as DataTexture;

  return (
    <DebugProvider debug={{sdf2d: {subpixel, contours, preprocess, postprocess}}}>

      <LinearRGB>
        <Flat>
          <Pass>
            <UI>
              <Layout>
                <Absolute top={"-40%"}>
                  <Flex direction="y" anchor={"center"} align={"center"} width={'100%'} height={'100%'}>

                    <RawTexture data={sdfTexture} render={(texture) =>
                      <Block width={w}>
                        <Block width={w} height={h} fill={[0.0, 0.0, 0.0, 1.0]} image={{
                          texture,
                          fit: 'scale',
                          repeat: 'none',
                        }} />
                      </Block>
                    }/>

                  </Flex>
                </Absolute>
              </Layout>
            </UI>
          </Pass>
        </Flat>
      </LinearRGB>

    </DebugProvider>
  );
};

