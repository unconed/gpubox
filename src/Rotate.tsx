import { use, useMemo, LC } from '@use-gpu/live';
import { HTML } from '@use-gpu/react';
import React from 'react';

type RotateProps = {
  size: number,
  color: string,
};

export const Rotate: LC<RotateProps> = (props: RotateProps) => {
  const {
    size = 20,
    color = '#fff',
  } = props;

  const root = document.body;

  const offset = 0.13 * size;
  const circle = 0.3 * size;
  const stem = 0.3 * size;

  return useMemo(() => (
    use(HTML, {
      root,
      style: {
        position: 'absolute',
        right: '5px',
        bottom: '5px',
      },
      children: (
        <div style={{
          position: 'relative',
          width: size,
          height: size,
        }}>
          <div style={{
            position: 'absolute',
            left: offset,
            top: offset,
            width: circle,
            height: circle,
            background: color,
            borderRadius: '100%',
          }} />

          <div style={{
            position: 'absolute',
            left: 0,
            width: stem,
            height: stem,
            borderLeft: `2px solid ${color}`,
            borderTop: `2px solid ${color}`,
            transform: `translate(-7px, 6px) rotate(-45deg)`,
            transformOrigin: '0px 0px',
          }} />
          <div style={{
            position: 'absolute',
            left: 0,
            width: stem,
            height: stem,
            borderLeft: `2px solid ${color}`,
            borderTop: `2px solid ${color}`,
            transform: `translate(19px, 6px) rotate(135deg)`,
            transformOrigin: '0px 0px',
          }} />

          <div style={{
            position: 'absolute',
            left: 0,
            width: stem,
            height: stem,
            borderLeft: `2px solid ${color}`,
            borderTop: `2px solid ${color}`,
            transform: `translate(6px, -7px) rotate(45deg)`,
            transformOrigin: '0px 0px',
          }} />
          <div style={{
            position: 'absolute',
            left: 0,
            width: stem,
            height: stem,
            borderLeft: `2px solid ${color}`,
            borderTop: `2px solid ${color}`,
            transform: `translate(6px, 19px) rotate(-135deg)`,
            transformOrigin: '0px 0px',
          }} />
        </div>
      ),
    })
  ), [size, color]);
};